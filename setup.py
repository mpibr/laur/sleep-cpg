from setuptools import setup, find_packages

setup(
    name='ihrem',
    version='2.0',
    description='Analysis code for pogona REM sleep',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
        ],
    }
)
