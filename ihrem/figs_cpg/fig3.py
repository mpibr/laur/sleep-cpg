"""
Sleep’s ultradian rhythm can be entrained by trains of light pulses, within a bounded range of inter-pulse intervals (IPIs).
"""
import logging

import numpy as np
import pandas as pd
import scipy.stats
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import timeslice
from ihrem.analysis import cycles
from ihrem.analysis import entrainment as ent
from ihrem.analysis.entrainment import PROT_COLORS
from ihrem.timeslice import ms, MS_TO_S


def savefig(f, name):
    splot.savefig(f, name=f'fig3/{name}')


def collect_rem_wins(exp_beta, all_trains):
    # limit rem/sws estimation to around the protocol time, to avoid going too far into late evening/early morning
    valid_wins = timeslice.Windows(pd.DataFrame({
        'start': all_trains.groupby('exp_name')['start'].min() - ms(minutes=30),
        'stop': all_trains.groupby('exp_name')['stop'].max() + ms(minutes=30),
    }))

    valid_wins['ref'] = valid_wins['start']

    exp_rem_wins = {}

    for exp_name, win in pbar(valid_wins.iter_wins(), total=len(valid_wins)):
        exp_rem_wins[exp_name] = cycles.extract_rem_wins(
            exp_beta.sel(exp_name=exp_name).crop(win).get(),
        )

    return exp_rem_wins


def label_bad_trains(all_trains):
    bad = [
        ('GL1580_20231202_sleep', 2),
        ('GL1580_20231203_sleep', 2),
        ('GL1580_20231204_sleep', 2),
        ('GL1580_20231204_sleep', 3),
        ('GL1580_20231205_sleep', 2),
        ('GL1580_20231206_sleep', 2),
        ('GL1671_20240320_sleep', 2),
        ('GL1671_20240320_sleep', 3),
        ('GL1671_20240320_sleep', 4),
        ('GL1671_20240320_sleep', 5),
        ('GL1671_20240320_sleep', 6),
        ('GL1671_20240320_sleep', 7),
        ('GL1671_20240320_sleep', 8),
        ('GL1671_20240321_sleep', 3),
        ('GL1671_20240321_sleep', 5),
        ('GL1671_20240321_sleep', 6),
        ('GL1671_20240321_sleep', 7),
        ('GL1671_20240321_sleep', 8),
        ('GL1671_20240321_sleep', 9),
    ]

    good_trains = all_trains
    for exp_name, local_train in bad:
        good_trains = good_trains.sel(exp_name=exp_name, local_train=local_train, but=True)

    is_bad = pd.Series(True, index=all_trains.index)

    is_bad.loc[good_trains.index] = False

    return is_bad


def collect_all_pulses(all_light_wins, good_trains):
    all_pulses = []

    for train_id, train, props in good_trains.iter_wins_items():
        pulses = all_light_wins.sel(exp_name=props['exp_name'], train=props['local_train']).copy()

        pulses['train_id'] = train_id

        all_pulses.append(pulses.wins)

    all_pulses = timeslice.Windows(pd.concat(all_pulses, axis=0, ignore_index=True))

    return all_pulses


def plot_mean_cut(phases_cut, good_trains, circ=True):
    phases_cut['interval'] = good_trains['interval'].reindex(phases_cut['train_id']).values
    phases_cut['pulse_len'] = good_trains['pulse_len'].reindex(phases_cut['train_id']).values

    grouped = list(phases_cut.sort_values(['interval', 'pulse_len']).iter_grouped(['interval', 'pulse_len']))

    f, axs = plt.subplots(nrows=len(grouped), sharex='all', sharey='all', figsize=(2.5, 5))

    for i, ((interval, pulse_len), traces) in enumerate(grouped):

        ax = axs[i]

        group_color = PROT_COLORS[interval, pulse_len]

        if circ:
            mean = scipy.stats.circmean(traces.traces.values, low=0, high=1, axis=1)

        else:
            mean = traces.traces.median(axis=1).values

        mean = pd.Series(
            mean,
            index=traces.time,
        )

        ax.plot(
            traces.traces,
            linewidth=.25,
            alpha=.25,
            color='k',
        )

        ax.plot(
            mean,
            linewidth=1,
            color='xkcd:cyan',
        )

        splot.add_desc(
            ax,
            f'{ent.get_protocol_desc(interval, pulse_len)} (n={len(traces.index)})',
            loc='upper left', color=group_color,
            fontsize=4, bkg_color='none',
            va='bottom',
        )

        ax.axvline(0, linestyle='--', color='k', linewidth=.5)

        ax.set_ylabel('phase ($\phi$)', fontsize=6)

    for ax in axs[:-1]:
        ax.tick_params(bottom=False, labelbottom=False, which='both')
        ax.spines['bottom'].set_visible(False)

    splot.set_time_ticks(axs[-1], scale='seconds')

    return f


def get_circular_hists(phase_evolution, binsize):
    phase_bins = np.arange(-1, +2 + binsize, binsize)

    hists = {}

    for k, traces in phase_evolution.items():
        vals = traces.traces.values.ravel() % 1

        vals = np.concatenate([vals - 1, vals, vals + 1])

        h, _ = np.histogram(vals, bins=phase_bins)

        h = pd.Series(h, index=pd.IntervalIndex.from_breaks(phase_bins))

        hists[k] = h

    hists = pd.DataFrame(hists).rename_axis(columns=['interval', 'pulse_len'])

    return hists


def hide_outside(ax, xmin, xmax):
    for xrange in [xmin, 0], [1, xmax]:
        ax.fill_between(
            xrange,
            [0, 0],
            [1, 1],
            transform=ax.get_xaxis_transform(),
            facecolor='w',
            edgecolor='none',
            alpha=.75,
            zorder=2.25,
            clip_on=True,
        )


def plot_hists_fill_linear_spread(ax, hists, label):
    hists = hists.loc[-.25:1.25].iloc[1:]
    hide_outside(ax, -.25, 1.25)

    yscale = hists.values.max() * .75

    zorders = np.linspace(.1, 0., len(hists.columns))
    for i, (k, h) in enumerate(hists.items()):
        ax.bar(
            h.index.mid,
            h.values / yscale,
            width=h.index.length,
            bottom=i,
            facecolor=PROT_COLORS.loc[k],
            alpha=.95,
            zorder=2 + zorders[i],
        )

        ax.text(1, i, f'{k[0] * MS_TO_S:,g}s', ha='right', fontsize=6, transform=ax.get_yaxis_transform())

    phase_ticks = np.linspace(0, 1, 5)
    ax.set_xticks(phase_ticks)
    ax.set(
        xlabel='phase',
        ylabel=label,
    )
    ax.spines['bottom'].set_bounds([0, 1])
    ax.autoscale(enable=True, axis='x', tight=True)

    splot.add_yscale_bar(ax, vmax=1 / yscale)


def collect_train_period_lens_many(exp_rem_wins, valid_trains, rel_analysis_wins, edges='crop'):
    all_train_periods = []

    for name, definition in rel_analysis_wins.items():

        if definition is None:
            wins = valid_trains

        else:
            wins = valid_trains.around(*definition)

        train_periods = ent.collect_train_period_lens(
            exp_rem_wins,
            wins,
            edges=edges,
        )

        train_periods['when'] = name

        all_train_periods.append(train_periods)

    all_train_periods = timeslice.Windows.concat_list(all_train_periods)

    all_train_periods['length'] = all_train_periods.lengths()

    return all_train_periods


def get_period_stats(train_periods_prepos):
    def flatten_index(df, prefix):
        df.columns = [f'{prefix}_{cat}_{when}' for cat, when in df.columns]

    period_median_length = train_periods_prepos.groupby(['train_id', 'cat', 'when'])['length'].median().unstack(
        'train_id').T
    flatten_index(period_median_length, 'median_length')

    period_mean_length = train_periods_prepos.groupby(['train_id', 'cat', 'when'])['length'].mean().unstack(
        'train_id').T
    flatten_index(period_mean_length, 'mean_length')

    period_count = train_periods_prepos.groupby(['train_id', 'cat', 'when'])['length'].size().unstack(
        'train_id').T.fillna(0).astype(int)
    flatten_index(period_count, 'count')

    period_total = train_periods_prepos.groupby(['train_id', 'cat', 'when'])['length'].sum().unstack(
        'train_id').T.fillna(0)
    period_ratio = period_total / (period_total['rem'] + period_total['sws'])
    flatten_index(period_total, 'total')
    flatten_index(period_ratio, 'ratio')

    return pd.concat([
        period_median_length,
        period_mean_length,
        period_count,
        period_total,
        period_ratio,
    ], axis=1)


def assert_no_lights_in_baseline(baseline_wins, exp_light_wins):
    for k, win, props in pbar(baseline_wins.iter_wins_items(), total=len(baseline_wins)):
        exp_name = props['exp_name']
        if exp_light_wins[exp_name].sel(cat='on').contained_in(win).any():
            logging.error(f'Lights in baseline win: {exp_name}')


def plot_scatter_single(
        ax,
        df,
        x, y, base_x, base_y,
        ylabel,
        clip_range=(-np.inf, +np.inf),
        join_curve=False,
):
    fit_color = 'xkcd:dark gray'
    baseline_color = 'xkcd:silver'

    ax.scatter(
        df[x],
        df[y].clip(lower=clip_range[0], upper=clip_range[1]),
        facecolor=fit_color,
        zorder=1e6,
        edgecolor='w',
        linewidth=.5,
        alpha=.75,
        s=120,
        clip_on=False,
    )
    ax.scatter(
        df[base_x],
        df[base_y],
        facecolor=baseline_color,
        zorder=1e5,
        edgecolor='w',
        linewidth=.25,
        alpha=.65,
        marker='D',
        s=10,
        clip_on=False,
    )

    p_value_colors = [
        'k',
        'xkcd:blue green',
        'xkcd:kelly green',
        'xkcd:lime green',
    ]

    if not join_curve:

        for x_v, sdf in df.groupby(x):
            mean = sdf[y].mean()
            std = sdf[y].std()
            ax.plot(
                x_v + ms(seconds=6) * np.array([-.5, +.5]),
                [mean] * 2,
                linewidth=1,
                color='k',
                zorder=1e6,
            )
            ax.plot(
                [x_v] * 2,
                [mean - std, mean + std],
                linewidth=1,
                color='k',
                zorder=1e6,
            )

            statistic, p_value = scipy.stats.ttest_rel(sdf[base_y], sdf[y], alternative='two-sided')
            #             statistic, p_value = scipy.stats.wilcoxon(sdf[base_y], sdf[y], alternative='two-sided')

            print(f't={statistic:.3f}')
            print(f'p={p_value:.6f}')

            ax.text(
                x_v,
                1,
                f'n={len(sdf):,g}\np={p_value:.3f}\nt={statistic:.3f}\n{splot.p_value_stars(p_value)}',
                fontsize=4,
                rotation=0,
                transform=ax.get_xaxis_transform(),
                clip_on=False,
                va='bottom',
                ha='center',
                color=p_value_colors[splot.p_value_stars_level(p_value)],
                zorder=1e7,
            )

    else:

        ax.plot(
            df.groupby(x)[y].mean(),
            linewidth=.5,
            color='k',
            zorder=1e6,
        )

        splot.add_desc(
            ax,
            f'n={len(df):,g}',
            loc='lower right',
        )

    ax.axvline(df[base_x].mean(), color='xkcd:silver', linestyle='--', linewidth=.5)
    ax.axhline(df[base_y].mean(), color='xkcd:silver', linestyle='--', linewidth=.5)

    ax.set(xlabel='IPI(s)', ylabel=ylabel)


def plot_summary(trains, min_count=0, stat='median_length', clip_upper=140_000):
    f, axs = plt.subplots(figsize=(2.5, 6), nrows=3, sharex='all')

    ax = axs[0]

    plot_scatter_single(
        ax,
        trains,
        x='interval',
        y='corr_at_interval',
        base_x='interval_nat',
        base_y='corr_at_nat_base',
        ylabel='corr.',
        join_curve=True,
    )

    to_count = trains.sel_between(
        count_rem_within=(min_count, np.inf),
        count_sws_within=(min_count, np.inf),
        how='all')

    ax = axs[1]
    plot_scatter_single(
        ax,
        to_count,
        x='interval',
        y=f'{stat}_rem_within',
        base_x='interval_nat',
        base_y=f'{stat}_rem_baseline',
        ylabel='REM duration (s)',
        clip_range=(0, clip_upper),
    )

    ax = axs[2]
    plot_scatter_single(
        ax,
        to_count,
        x='interval',
        y=f'{stat}_sws_within',
        base_x='interval_nat',
        base_y=f'{stat}_sws_baseline',
        ylabel='NREM duration (s)',
        clip_range=(0, clip_upper),
    )

    axs[0].set(
        ylim=(-.5, 1),
        yticks=[-.5, 0, .5, 1],
    )

    for ax in [axs[1], axs[2]]:
        yticks = np.arange(0, clip_upper + 1, ms(seconds=20)).astype(int)
        ylabels = [str(t) for t in (yticks * MS_TO_S).astype(int)]
        #         if (to_count[f'{stat}_sws_within'] > clip_upper).any() or (to_count[f'{stat}_rem_within'] > clip_upper).any():
        #             ylabels[-1] = '≥' + ylabels[-1]

        ax.set_yticks(
            yticks,
            labels=ylabels,
            minor=False,
        )

        yticks = np.arange(0, clip_upper + 1, ms(seconds=10)).astype(int)
        ax.set_yticks(
            yticks,
            minor=True,
        )

        ax.set(
            ylim=(0, clip_upper),
        )

    for ax in axs.ravel():
        splot.set_time_ticks(
            ax, scale='seconds', tight=False, label='IPI (s)', major=ms(seconds=40),
            minor=ms(seconds=20)
        )
        ax.tick_params(labelbottom=True)

    return f
