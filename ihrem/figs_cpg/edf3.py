"""
Ambient light pulses cause neither nuchal electromyographic (EMG) activity nor eye movement
"""
import numpy as np
import pandas as pd
import scipy.stats
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import timeslice
from ihrem import traces as tr
from ihrem.timeslice import ms, Win


def load_all_emg_traces(reg_sel, all_load_wins, channels=None):
    if channels is None:
        channels = slice(None)

    all_emg_traces = []

    all_emg_channels = reg_sel['EMG'].dropna()

    for exp_name, emg_channels in pbar(all_emg_channels.items(), total=len(all_emg_channels), desc='exp'):

        wins = all_load_wins.sel(exp_name=exp_name)

        if len(wins) > 0:
            emg_channels = [int(ch) for ch in emg_channels.split('+')]
            emg_channels = np.array(emg_channels) - 1
            emg_channels = emg_channels[channels]

            loader = reg_sel.get_loader(exp_name)

            traces = tr.Traces.from_loader(
                loader,
                wins,
                channels=emg_channels,
                load_hz=1000,
                pbar=pbar,
            )
            traces['ch'] = traces['channel'].map({ch: i for i, ch in enumerate(emg_channels)})

            all_emg_traces.append(traces)

    all_emg_traces = tr.Traces.concat_list(all_emg_traces)

    return all_emg_traces


def get_emg_rectified(
        emg_traces,
        filter_hz=(100, 225),
):
    # smooth
    emg_traces = emg_traces.filter_pass(filter_hz)

    # rectify
    emg_traces = emg_traces.abs()

    return emg_traces


def get_emg_integral(
        emg_rect,
        integral_win=ms(seconds=20),
):
    integral_win_discrete = int(integral_win / emg_rect.sampling_period)

    emg_int = emg_rect.sum_rolling(integral_win_discrete, min_periods=integral_win_discrete)

    return emg_int


def plot_emg(pre, pos, awake, stats, suptitle='', exps=None):
    colors = {
        'pre': 'xkcd:silver',
        'pos': splot.COLORS['pulse'],
        'awake': 'xkcd:cerulean',
    }

    if exps is None:
        exps = pre['exp_name'].unique()

    f, axs = plt.subplots(nrows=len(exps), sharex='all', figsize=(5, 5), sharey='all', squeeze=True)

    f.suptitle(suptitle)

    for i, exp_name in enumerate(pbar(exps)):
        ax = axs.ravel()[i]

        stats_desc = (
                'pre-post:\n'
                + stats.loc[exp_name, 'pre_pos_desc']
                + '\n\npost-awake:\n'
                + stats.loc[exp_name, 'pos_awake_desc']
        )

        splot.add_desc(
            ax,
            stats_desc,
            loc='upper left',
        )

        for desc, traces in [('pre', pre), ('pos', pos), ('awake', awake)]:
            traces = traces.sel(exp_name=exp_name, ch=0)

            q0 = traces.quantile(.25, axis=1)
            q1 = traces.quantile(.75, axis=1)

            ax.fill_between(
                q0.index,
                q0.values,
                q1.values,
                facecolor=colors[desc],
                alpha=.5,
            )

            ax.plot(
                traces.median(axis=1),
                color=colors[desc],
                label=f'{desc} n={len(traces.index):,g}',
            )

        ax.legend(loc='upper right', fontsize=6)
        ax.set(title=exp_name)

    splot.drop_spines_grid(axs)

    ax = axs[-1]
    splot.set_time_ticks(
        ax,
        scale='minutes',
        label='Time (min)',
    )
    ax.set(
        # ylim = (1.5, 3),
        # yticks=[1.5, 2, 2.5, 3]
    )
    for ax in axs[:]:
        ax.set_ylabel('uV')

    return f


def plot_dlc_vs_pulses(exp_eyes, eyes_cut, analysis_windows):
    f, axs = plt.subplots(ncols=3, nrows=eyes_cut['exp_name'].nunique(), sharey='all', sharex='col', figsize=(6, 6))

    for i, exp_name in enumerate(pbar(eyes_cut['exp_name'].unique())):

        traces = exp_eyes.sel(exp_name=exp_name).crop((0, ms(minutes=6)))
        traces = traces.get_df('side')

        ax = axs[i, 0]
        for side, trace in traces.items():
            ax.plot(trace, color=splot.COLORS[side])

        splot.add_desc(
            ax,
            f'{exp_name}'
        )

        wins = analysis_windows.sel(exp_name=exp_name)

        pulses = timeslice.Windows(
            wins[['pulse_start', 'pulse_stop', 'ref']].rename(columns=dict(pulse_start='start', pulse_stop='stop'))
        )

        pulses = pulses.shift(-pulses['ref'])

        for j, side in enumerate(['left', 'right']):
            traces = eyes_cut.sel(exp_name=exp_name, side=side)
            ax = axs[i, 1 + j]
            ax.plot(traces.traces, color=splot.COLORS[side], linewidth=.25, alpha=.5)
            ax.plot(traces.traces.mean(axis=1), color=splot.COLORS[side], linewidth=1)

            splot.add_desc(
                ax,
                f'(n={len(traces.index):,g})'
            )

            ax.axvline(0, color='xkcd:golden', linestyle='--')

            ax.fill_between(
                pulses.drop_duplicates().get_rel_win(),
                [0, 0],
                [1, 1],
                transform=ax.get_xaxis_transform(),
                facecolor='xkcd:golden',
                alpha=0.25,
            )

    for ax in axs[-1, :]:
        splot.set_time_ticks(ax, scale='minutes')

    splot.add_yscale_bar(axs[-1, 0])

    axs[-1, 0].set_xlim(0, axs[-1, 0].get_xlim()[1])

    axs[0, 0].set(title='first 30min of\nrecording (awake)')
    axs[0, 1].set(title='left eye\naround pulses (mean)')
    axs[0, 2].set(title='right eye\naround pulses (mean)')

    splot.drop_spines_grid(axs, left_edge=True)

    return f


def collect_awake_analysis_windows(reg, exp_light_wins, win_len):
    awake_wins = {}

    for exp_name, light_wins in pbar(exp_light_wins.items(), total=len(exp_light_wins)):

        loader = reg.get_loader(exp_name)

        light_wins = timeslice.Windows(light_wins.wins)

        lights = light_wins.sel(cat='on', is_pulse=False)

        valid_win = Win(ms(hours=0), ms(hours=2))

        to_consider = {
            'early': valid_win.shift(loader.win_ms.start),
            'late': valid_win.shift(loader.win_ms.stop - valid_win.length)
        }

        for when, win in to_consider.items():

            sel = lights.crop_to_main(win)

            if len(sel) > 0:
                awake_wins[exp_name, when] = sel.fragment(win_len)

                awake_wins[exp_name, when]['ref'] = awake_wins[exp_name, when].mid()

    return timeslice.Windows.concat(awake_wins, cycle_name=['exp_name', 'when'])


def extract_emg_stats(reg_sel, pre_pulse_int, pos_pulse_int, awake_int_half) -> pd.DataFrame:
    assert np.all(pre_pulse_int['win_idx'] == pos_pulse_int['win_idx'])

    stats = {}

    for exp_name in reg_sel.index:
        sel_pre = pre_pulse_int.sel(exp_name=exp_name)['summary']
        sel_pos = pos_pulse_int.sel(exp_name=exp_name)['summary']
        sel_awake = awake_int_half.sel(exp_name=exp_name)['summary']

        assert len(sel_pre) == len(sel_pos)

        stat, p_value = scipy.stats.wilcoxon(sel_pre, sel_pos)
        stats[exp_name, 'pre_pos_stat'] = stat
        stats[exp_name, 'pre_pos_p_value'] = p_value
        stats[exp_name, 'pre_pos_n'] = (len(sel_pre), len(sel_pos))
        stats[exp_name, 'pre_pos_desc'] = (
            f'W={stat:,g}; p={p_value:.5f} {splot.p_value_stars(p_value)} n={len(sel_pos)}'
        )

        stat, p_value = scipy.stats.mannwhitneyu(sel_pos, sel_awake)
        stats[exp_name, 'pos_awake_stat'] = stat
        stats[exp_name, 'pos_awake_p_value'] = p_value
        stats[exp_name, 'pos_awake_n'] = (len(sel_pos), len(sel_awake))
        stats[exp_name, 'pos_awake_desc'] = (
            f'U={stat:,g}; p={p_value:.5f} {splot.p_value_stars(p_value)} n={(len(sel_pos), len(sel_awake))}'
        )

    stats = pd.Series(stats)
    stats.index.names = ['exp_name', 'which']

    stats = stats.unstack('which')

    return stats
