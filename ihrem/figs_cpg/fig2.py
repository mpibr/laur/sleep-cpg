"""
The reset of the ultradian rhythm is phase dependent
"""
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import paths
from ihrem import plot as splot
from ihrem import timeslice
from ihrem.analysis import prc_plot
from ihrem.analysis import sne
from ihrem.timeslice import ms


def savefig(f, name):
    splot.savefig(f, name=f'fig2/{name}')


def plot_beta_by_pulse_len(beta_cut):
    pulse_lens = np.sort(beta_cut['pulse_len'].unique())

    f, axs = plt.subplots(
        nrows=len(pulse_lens), sharex='all', sharey='all',
        figsize=(3, 2.5),
    )

    for i, pulse_len in enumerate(pulse_lens):
        pulse_len: float

        ax = axs[i]

        sel = beta_cut.sel(pulse_len=pulse_len)

        sel.plot_overlaid(ax, color='xkcd:grey', linewidth=.25, alpha=.25)
        ax.plot(sel.median(axis=1), color='k', linewidth=1, alpha=1)

        splot.add_desc(ax, f'{pulse_len / ms(seconds=1):,g}s', loc='upper left', bkg_color='none')
        splot.add_desc(ax, f'n={len(sel.index):,g}', loc='upper right', bkg_color='none')

        splot.plot_pulse_shade(ax, (-pulse_len, 0))

        ax.axvline(0, color='xkcd:dark grey', linestyle='--')

    splot.drop_spines_grid(axs, left_edge=True)
    ax = axs[-1]
    splot.add_yscale_bar(ax)
    splot.set_time_ticks(ax, scale='minutes', major=ms(minutes=1), label='Time (min)')

    return f


def load_reg_healthy_no_cap_light_pulses():
    reg_full = paths.Registry.read_excel()

    reg_sel = reg_full.sel_mask(
        (reg_full['lesion'] != 'BST')
        &
        reg_full['lights'].notna()
        &
        reg_full['stim'].isin(['light pulses'])
        &
        reg_full['state'].isin(['sleep'])
        &
        reg_full.is_bilat('CLA')
        &
        reg_full['cap'].isna()
    )

    print(f'Taking {len(reg_sel)}/{len(reg_full)} experiments')

    return reg_sel


def plot_phase_dependency(beta_comb_cut, phase_comb_cut, class_by='phase_comb_cat', shaded=False):
    beta_comb_cut = beta_comb_cut.crop((ms(minutes=-4), ms(minutes=5)))

    f, axs = plt.subplots(ncols=3, figsize=(7, 2.5))

    ax = axs[0]
    prc_plot.plot_betas_grouped_single(
        ax,
        beta_comb_cut.sel(pulse_len=ms(seconds=1)),
        class_by=class_by,
        shaded=shaded,
    )

    ax = axs[1]
    prc_plot.plot_phases_grouped_single(
        ax,
        phase_comb_cut.sel(pulse_len=ms(seconds=1)),
        class_by=class_by,
        shaded=False,
    )

    ax = axs[2]

    prc_plot.plot_betas_grouped_single(
        ax,
        beta_comb_cut.sel(pulse_len=ms(seconds=1)),
        class_by=class_by,
        show_labels=False,
        shaded=shaded,
    )

    prc_plot.plot_betas_grouped_single(
        ax,
        beta_comb_cut.sel(pulse_len=ms(seconds=90)),
        class_by=class_by,
        colors={
            'late sws': 'xkcd:dark grey',
            'early sws': 'xkcd:dark grey',
            'late rem': 'xkcd:dark grey',
            'early rem': 'xkcd:dark grey',
        },
        show_labels=True,
        shaded=shaded,
    )

    ax.plot(
        [0, ms(seconds=90)],
        [1, 1],
        transform=ax.get_xaxis_transform(),
        color=splot.COLORS['pulse'],
        zorder=16,
        clip_on=False,
        solid_capstyle='butt',
        linewidth=3,
    )

    return f


def load_multi_sns(reg):
    exp_sns = {}

    for exp_name in pbar(reg.experiment_names):
        try:
            sns = reg.load_all_sne(exp_name, suffix='_cdf')
            sns = sns[sns['null_cdf'].between(-np.inf, 0.05)]

            sns = sne.SharpNegativeEvents(sns).patch_simplified_channels()

            exp_sns[exp_name] = sns

        except FileNotFoundError:
            pass

    return exp_sns


def assign_sns_cat_multi(exp_sns, exp_light_wins, valid_win):
    for exp_name in pbar(exp_sns.keys()):
        light_wins = exp_light_wins[exp_name].crop_to_main(valid_win)

        sns = exp_sns[exp_name]

        sns['light_cat'] = light_wins.classify_events(sns['ref_time'])['cat']


def plot_sn_stats_combined(exp_light_wins, exp_sns, exp_names, valid_win, ch=0):
    pooled_sns = []

    pulse_counts = pd.Series(0, index=['on', 'off'])
    pulse_durations = pd.Series(0, index=['on', 'off'])

    for exp_name in exp_names:
        light_wins = exp_light_wins[exp_name]
        light_wins = light_wins.crop_to_main(valid_win)

        pulse_counts = pulse_counts + light_wins['cat'].value_counts()
        pulse_durations = pulse_durations + light_wins.total_by_cat()

        sns = exp_sns[exp_name].sel(channel=ch).sel_between(ref_time=valid_win)

        pooled_sns.append(sns.reg)

    pooled_sns = pd.concat(pooled_sns, axis=0)

    colors = {
        'on': 'xkcd:golden',
        'off': 'k',
    }

    by = 'light_cat'

    attributes = {
        'amplitude': pick_bins(pooled_sns['amplitude']),
        'duration': np.arange(0, pooled_sns['duration'].max(), 1),
        'iei': np.linspace(0, 150, 75 + 1),
    }

    f, axs = plt.subplots(ncols=len(attributes), figsize=(4, 1.5))

    axs_dict = dict(zip(attributes.keys(), axs))

    for attribute, ax in axs_dict.items():

        values = pooled_sns[attribute]

        bins = attributes[attribute]

        bin_centers = (bins[:-1] + bins[1:]) * .5

        for cat, sel in values.groupby(pooled_sns[by]):
            sel = sel.dropna()

            h, _ = np.histogram(sel, bins=bins)

            h = h / h.sum()

            ax.plot(
                bin_centers,
                h,
                color=colors[cat],
                label=f'n={len(sel):,g}'
            )

            ax.scatter(
                [sel.median()],
                [0],
                marker='v',
                facecolor=colors[cat],
                alpha=0.5,
            )

        ax.set(title=attribute)

        splot.add_yscale_bar(ax, vmax=.01, desc='1%')
        ax.set(xlim=(bins.min(), bins.max()))

    splot.set_time_ticks(
        axs_dict['amplitude'],
        major=500,
        minor=100,
        label='uV',
    )
    axs_dict['amplitude'].legend(loc='upper left', fontsize=4)
    axs_dict['iei'].legend(loc='upper right', fontsize=4)
    axs_dict['duration'].legend(loc='upper right', fontsize=4)

    pulse_desc = '\n'.join([
        f'{pulse_counts[cat]} {cat.upper()} periods (total: {timeslice.strf_ms(pulse_durations[cat])})'
        for cat in ('on', 'off')
    ])
    ax = axs_dict['amplitude']
    ax.text(
        0, .5,
        f'{len(exp_names)} recordings (probe {ch})\n{pulse_desc}',
        transform=ax.transAxes, fontsize=4, clip_on=False
    )

    splot.set_time_ticks(
        axs_dict['iei'],
        major=40,
        minor=10,
        label='ms',
    )
    splot.set_time_ticks(
        axs_dict['duration'],
        major=10,
        minor=1,
        label='ms',
    )

    for attribute, ax in axs_dict.items():
        ax.set(xlim=(attributes[attribute].min(), attributes[attribute].max()))

    return f


def pick_bins(values, count=50):
    step = values.sort_values().diff().replace(0, np.nan).min()

    vmin = values.min()
    vmax = values.max()

    ideal_step = (vmax - vmin) / count
    step = np.round(ideal_step / step) * step

    bins = np.arange(
        vmin - .5 * step,
        vmax + step,
        step,
    )

    return bins
