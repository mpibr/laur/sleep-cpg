"""
The reset of sleep’s ultradian rhythm is unilateral if light affects a single eye.
The two sides, however, rapidly resynchronize.
"""
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import paths
from ihrem import plot as splot
from ihrem import timeslice
from ihrem.analysis import prc_plot
from ihrem.timeslice import ms


def savefig(f, name):
    splot.savefig(f, name=f'fig5/{name}')


def load_reg():
    reg_full = paths.Registry.read_excel()

    reg_sel = reg_full.sel_mask(
        (reg_full['lesion'] != 'BST')
        &
        reg_full['lights'].notna()
        &
        reg_full['stim'].isin([
            'light pulses',
        ])
        &
        reg_full['state'].isin(['sleep'])
        &
        reg_full.is_bilat('CLA')
    )

    reg_sel['cap'].fillna('no', inplace=True)

    reg_sel['cap_mode'] = label_cap_mode(reg_sel)

    print(f'Taking {len(reg_sel)}/{len(reg_full)} experiments')

    return reg_sel


def label_seeing(reg) -> pd.Series:
    return reg[['cap', 'side']].apply(lambda row: {
        ('left', 'right'): 'blind',
        ('right', 'left'): 'blind',
        ('left', 'left'): 'seeing',
        ('right', 'right'): 'seeing',
        ('no', 'right'): 'seeing',
        ('no', 'left'): 'seeing',
        ('bilat', 'right'): 'blind',
        ('bilat', 'left'): 'blind',
    }[row['cap'], row['side']], axis=1)


def label_cap_mode(reg) -> pd.Series:
    return reg['cap'].map({
        'left': 'unilat',
        'right': 'unilat',
        'bilat': 'bilat',
        'no': 'no',
    })


def plot_lead_traces_barcode(lead_traces_sorted, highlight=(), figsize=(3, 5)):
    f, ax = plt.subplots(figsize=figsize)

    y_offset = 0

    y_step = -1

    for cap_mode in pbar(['bilat', 'unilat', 'no']):

        by_cap = lead_traces_sorted.sel(cap_mode=cap_mode)

        splot.add_desc(
            ax,
            f' {cap_mode}-cap (n={len(by_cap.index):,g})',
            loc='upper left',
            x=0,
            y=y_offset,
            transform=ax.get_yaxis_transform(),
            fontsize=6,
            bkg_alpha=.95,
        )

        for phase_cat in ['early sws', 'late sws', 'early rem', 'late rem']:

            traces = by_cap.sel(phase_comb_cat=phase_cat)

            splot.add_desc(
                ax,
                f' {phase_cat} (n={len(traces.index):,g})',
                loc='upper right',
                x=1,
                y=y_offset,
                transform=ax.get_yaxis_transform(),
                color=prc_plot.PHASE_CAT_COLORS[phase_cat],
                fontsize=6,
                bkg_alpha=.95,
            )

            for j, (k, trace) in enumerate(pbar(traces.traces.items(), total=len(traces.index))):
                splot.plot_wins_rectangle(
                    ax,
                    timeslice.Windows.build_from_contiguous_values(trace),
                    colors=prc_plot.CAP_COLORS,
                    y0=y_offset,
                    y1=y_offset + y_step,
                    transform=ax.transData,
                )

                if k in highlight:
                    ax.scatter(
                        [ms(minutes=-2)],
                        [y_offset + .5 * y_step],
                        marker='>',
                        clip_on=False,
                        facecolor=splot.COLORS['pulse'],
                        edgecolor='k',
                        linewidth=.25,
                        # transform=ax.get_yaxis_transform(),
                        s=20,
                        zorder=1e6,
                        alpha=1,
                    )

                y_offset += 1 * y_step

            y_offset += 2 * y_step

        y_offset += 6 * y_step

    ax.axvline(0, color=splot.COLORS['pulse'], linestyle='--')
    splot.set_time_ticks(
        ax,
        scale='minutes',
        major=ms(minutes=1),
        lim=lead_traces_sorted.get_rel_win(),
        label='Time (min)',
    )
    ax.set(
        ylim=(y_offset + y_step, -y_step),
    )

    splot.drop_spine(ax, which='y')

    return f


def combine_dom_and_rem_traces(dom_traces, rem_traces):
    assert np.all(rem_traces.index == dom_traces.index)
    assert np.all(rem_traces[['exp_name']] == dom_traces[['exp_name']])

    common_win = (
        max(rem_traces.time.min(), dom_traces.time.min()),
        min(rem_traces.time.max(), dom_traces.time.max()),
    )

    rem_traces = rem_traces.crop(common_win)
    dom_traces = dom_traces.crop(common_win)

    lead_traces = dom_traces.applymap({'left': -1, 'right': +1}) * rem_traces.applymap({'rem': +1, 'sws': 0}).traces
    lead_traces = lead_traces.applymap({0: 'sws', -1: 'left', 1: 'right'})

    return lead_traces


def label_lead_seeing(lead_traces_unilat, k):
    trace = lead_traces_unilat.get(k).copy()

    cap = lead_traces_unilat.loc[k, 'cap']
    other = dict(left='right', right='left')[cap]

    trace.loc[trace == cap] = 'seeing'
    trace.loc[trace == other] = 'blind'

    return trace


def sort_lead_unilat(lead_traces):
    lead_traces_unilat = lead_traces.sel(cap_mode='unilat')

    lead_traces_unilat = lead_traces_unilat.replace_traces(
        pd.DataFrame({
            k: label_lead_seeing(lead_traces_unilat, k)
            for k in pbar(lead_traces_unilat.index)
        })
    )

    assert np.all(lead_traces_unilat.traces.isin(['sws', 'seeing', 'blind']))

    lead_traces_sorted = lead_traces.copy()
    lead_traces_sorted.traces.loc[:, lead_traces_unilat.index] = lead_traces_unilat.traces

    return lead_traces_sorted


def extract_time_to_first_sws(sel, sws_len_thresh=ms(seconds=40), min_delay=0):
    time_to_first_sws = {}

    for k, trace in pbar(sel.traces.items(), total=len(sel.index)):
        periods = timeslice.Windows.build_from_contiguous_values(trace)

        candidates = periods.sel(cat='sws').crop_to_main((min_delay, np.inf))
        candidate_idcs = candidates.index[candidates.lengths() >= sws_len_thresh]

        if len(candidate_idcs) > 0:
            time_to_first_sws[k] = periods.loc[candidate_idcs, 'stop'].min()

        else:
            time_to_first_sws[k] = np.inf

    return pd.Series(time_to_first_sws)


def plot_time_to_sws_vs_phase(
        ax,
        strong_traces_sorted,
        highlight=None,
        facecolor='xkcd:grey',
        show_rolled=True,
):
    strong_traces_sorted = strong_traces_sorted.shuffle()
    phases = strong_traces_sorted['phase_comb']

    times = strong_traces_sorted['time_to_first_sws']
    times = times.replace(np.inf, np.nan)

    if highlight is not None:
        mask = strong_traces_sorted.loc[times.index, highlight]
    else:
        mask = np.zeros(len(strong_traces_sorted.index), dtype=bool)

    for highlight, mask in (False, ~mask), (True, mask):

        if np.any(mask):
            ax.scatter(
                phases[mask],
                times[mask],
                facecolor=facecolor,
                edgecolor='k' if highlight else 'w',
                linewidth=.5,
                alpha=.5,
                s=60,
                zorder=1e5 if highlight else 1e4,
            )

    if show_rolled:
        grouped = times.groupby(pd.cut(phases, bins=np.linspace(0, 1, 21)))

        trace = grouped.median()
        trace.index = pd.IntervalIndex(trace.index).mid
        trace.sort_index(inplace=True)

        ax.plot(
            trace.index,
            trace.values,
            color='k',
            zorder=1e6,
        )

    splot.set_time_ticks(ax, which='y', scale='minutes', tight=False, label='Time (min)')

    ax.set(
        xlabel=r'phase ($\varphi$)',
        title=f'time-of-first-synch (n={len(times):,g})',
        #         xticks=[0, .25, .5, .75, 1],
    )
    ax.set_xticks([0, .5, 1])
    ax.set_xticks([.25, .75], minor=True)
    ax.spines['bottom'].set_bounds(0, 1)

    ax.tick_params(labelleft=True)


def plot_time_to_sws_vs_phase_multi(strong_traces_sorted):
    f, axs = plt.subplots(ncols=3, sharex='all', sharey='all', figsize=(5, 2.))

    for i, cap_mode in enumerate(['unilat', 'no', 'bilat']):
        ax = axs[i]
        plot_time_to_sws_vs_phase(
            axs[i],
            strong_traces_sorted.sel(cap_mode=cap_mode),
            highlight=None,
        )
        ax.set_title(f'{cap_mode}-cap\n{ax.get_title()}')

    return f


def plot_colored(ax, strong_traces_sorted, color_by):
    for group, count in strong_traces_sorted.value_counts(color_by).sort_values(ascending=False).items():
        sel = strong_traces_sorted.sel(**{color_by: group})

        ax.scatter(
            sel['phase_comb'],
            sel['time_to_first_sws'],
            edgecolor='w',
            linewidth=.5,
            alpha=.5,
            s=60,
            label=f'{group} (n={count:,g})',
        )

    ax.legend(loc='upper right', fontsize=4)

    ax.set_title(f'colored by {color_by}')

    ax.set(
        xlabel=r'phase ($\varphi$)',
    )
    ax.set_xticks([0, .5, 1])
    ax.set_xticks([.25, .75], minor=True)
    ax.spines['bottom'].set_bounds(0, 1)


def plot_time_to_sws_vs_other(strong_traces_sorted, cap_mode):
    sel = strong_traces_sorted.sel(cap_mode='unilat')

    f, axs = plt.subplots(ncols=3, sharex='none', sharey='all', figsize=(5, 2.))

    ax = axs[0]
    plot_time_to_sws_vs_phase(
        ax,
        sel,
    )
    ax.set_title(f'{cap_mode}-cap\n{ax.get_title()}')

    ax = axs[1]
    plot_colored(
        ax,
        sel,
        color_by='animal',
    )

    ax = axs[2]

    ax.scatter(
        sel['ref'],
        sel['time_to_first_sws'],
        facecolor='xkcd:grey',
        edgecolor='w',
        linewidth=.5,
        alpha=.5,
        s=60,
    )

    trace = sel['time_to_first_sws'].groupby(
        pd.cut(sel['ref'], bins=np.arange(ms(hours=2), ms(hours=10), ms(hours=0.25)))).median()
    trace.index = pd.IntervalIndex(trace.index).mid
    trace.sort_index(inplace=True)

    ax.plot(
        trace.index,
        trace.values,
        color='k',
        zorder=1e6,
    )

    splot.set_time_ticks(ax, scale='hours', tight=False)
    ax.set_title(f'by time of the night')

    splot.drop_spines_grid(axs, rows=False)

    return f
