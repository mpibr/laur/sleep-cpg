"""
REMP sleep homeostasis
"""
import numpy as np
import pandas as pd
import scipy.stats
from matplotlib import pyplot as plt

from ihrem import plot as splot
from ihrem import timeslice
from ihrem.timeslice import MS_TO_S, ms


def plot_single_trains(axs, good_trains, exp_beta, exp_light_wins):
    zoom_wins = good_trains.extend(pre=ms(minutes=-30), post=ms(minutes=+30))

    beta_cut = exp_beta.cut_merge(zoom_wins).downsample(1000)

    for i, k in enumerate(beta_cut.index):
        ax = axs[i]

        zoom_win = zoom_wins.get(k)
        train_win = good_trains.get(k).shift(-zoom_wins.loc[k, 'ref'])

        ax.fill_between(
            train_win,
            [0, 0],
            [1, 1],
            facecolor='xkcd:magenta',
            alpha=0.125,
            transform=ax.get_xaxis_transform(),
        )

        exp_name = beta_cut.loc[k, 'exp_name']
        local_train = beta_cut.loc[k, 'local_train']

        light_wins = exp_light_wins[exp_name]
        light_wins = light_wins.crop_to_main(zoom_win).shift(-zoom_wins.loc[k, 'ref'])

        trace = beta_cut.get(k)

        ax.plot(
            light_wins.generate_cat_contiguous(100).map({'on': +.25, 'off': 0}) + 1.75,
            color='xkcd:golden',
            clip_on=False,
        )

        ax.fill_between(
            trace.index,
            np.zeros(len(trace)),
            trace.values,
            facecolor='k',
        )

        splot.add_desc(
            ax,
            f'{k} - {exp_name} train #{local_train}',
            fontsize=5,
        )

        ax.set(ylim=(0, 2))

    ax = axs[-1]
    splot.set_time_ticks(
        ax,
        scale='minutes',
        major=ms(minutes=5),
    )

    splot.drop_spines_grid(axs, left_edge=True)

    splot.add_yscale_bar(axs.ravel()[-1])

    axs.ravel()[-1].spines['bottom'].set_position(('outward', 2))


def plot_many_trains(
        all_trains,
        exp_beta,
        exp_light_wins,
):
    counts = all_trains.groupby('interval').size()

    f, axs = plt.subplots(
        sharex='col', sharey='col', nrows=counts.max(), ncols=len(counts), figsize=(9, 5), squeeze=False)

    for j, interval in enumerate(np.sort(all_trains['interval'].unique())):
        plot_single_trains(
            axs[:, j],
            all_trains.sel(interval=interval),
            exp_beta,
            exp_light_wins,
        )

    return f


def plot_sumamries(supp, standard):
    metrics = [
        'total_rem',
        'mean_length_rem',
        'mean_length_sws',
    ]

    f, axs = plt.subplots(ncols=len(metrics), nrows=2, figsize=(5, 3), squeeze=False, sharex='all', sharey='col')

    for j, metric in enumerate(metrics):

        xcol, ycol = f'{metric}_pre', f'{metric}_pos'

        for i, sel in enumerate([supp, standard]):

            ax = axs[i, j]

            splot.wilcoxon_test(ax, sel[xcol], sel[ycol], sig_color='r')

            for k, ((interval, pulse_len), subsel) in enumerate(sel.groupby(['interval', 'pulse_len'])):

                if subsel['standard'].all():
                    color = 'k'

                else:
                    color = f'C{k}'

                ax.plot(
                    subsel[[xcol, ycol]].values.T,
                    '.-',
                    color=color,
                    clip_on=False,
                    alpha=.5,
                    markeredgewidth=0,
                )

                desc = f'{pulse_len * MS_TO_S:,g}s @ {interval * MS_TO_S:,g}'

                ax.text(
                    1,
                    subsel[ycol].max(),
                    desc,
                    color=color,
                    fontsize=5,
                    ha='right',
                )

            splot.set_time_ticks(ax, which='y', scale='seconds', tight=False)
            ax.set(title=metric.replace('_', '\n') + '\n')

    splot.drop_spines_grid(axs, left=False, left_label=False)

    for ax in axs[-1, :]:
        ax.set(xticks=[0, 1], xticklabels=['pre', 'post'])

    return f


def upsample_lengths(rem_wins, sampling_period=ms(seconds=1), win=None):
    if win is None:
        win = rem_wins.get_global_win()

    ts = win.arange(sampling_period)

    lengths = pd.Series(
        rem_wins.lengths().values,
        index=rem_wins.mid().values,
    )

    up = scipy.interpolate.interp1d(
        lengths.index,
        lengths.values,
        kind='nearest',
        fill_value='extrapolate',
    )(ts)

    up = pd.Series(up, index=ts)

    return up


def plot_experiment(rem_wins, light_wins, comb_beta, analysis_wins, upsampled_lengths):
    rem_colors = dict(rem='k', sws='xkcd:silver', cycle='xkcd:purple')

    f, axs = plt.subplots(nrows=3, sharex='all', figsize=(10, 2), gridspec_kw=dict(height_ratios=[1, 2, 5]))

    ax = axs[0]

    ax.plot(
        light_wins.generate_cat_contiguous(100).map({'on': 1, 'off': 0}),
        color='k',
    )

    ax = axs[0]
    splot.plot_wins_fill(ax, analysis_wins.sel(cat='pre'), facecolor='xkcd:grey')
    splot.plot_wins_fill(ax, analysis_wins.sel(cat='betw'), facecolor='xkcd:magenta')
    splot.plot_wins_fill(ax, analysis_wins.sel(cat='post'), facecolor='xkcd:cyan')
    splot.plot_wins_fill(ax, analysis_wins.sel(cat='post+1h'), facecolor='xkcd:green')
    splot.plot_wins_fill(ax, analysis_wins.sel(cat='post+2h'), facecolor='xkcd:blue')

    splot.drop_spine(ax, 'left')
    splot.add_desc(ax, 'light', loc='upper left', x=0.025)

    ax = axs[1]
    splot.add_yscale_bar(ax)
    splot.add_desc(ax, 'comb. beta', loc='upper left', x=0.025)

    comb_beta = comb_beta.iloc[::5]

    print(np.diff(comb_beta.index))

    ax.fill_between(
        comb_beta.index,
        np.zeros(len(comb_beta)),
        comb_beta.values,
        facecolor='k',
        alpha=1,
    )

    ax = axs[2]
    splot.add_desc(ax, 'REM/SW durations', loc='upper left', x=0.025)

    clip_at = ms(minutes=4)

    for cat in 'rem', 'sws':
        wins = rem_wins.sel(cat=cat)

        valid = wins.lengths() <= clip_at

        ax.scatter(
            wins.mid()[valid],
            wins.lengths()[valid],
            facecolor=rem_colors[cat],
            clip_on=False,
            s=100,
            alpha=.75,
            zorder=1e6,
        )

        ax.scatter(
            wins.mid()[~valid],
            wins.lengths()[~valid].clip(upper=clip_at),
            edgecolor=rem_colors[cat],
            facecolor='w',
            clip_on=False,
            zorder=1e6,
        )

        ax.plot(upsampled_lengths[cat].rolling(60 * 10, center=True).mean(), color=rem_colors[cat])

    splot.set_time_ticks(ax, which='y', scale='seconds')

    ticks = np.arange(0, clip_at + 1, ms(minutes=1))
    ax.set_yticks(ticks)
    ax.set_yticklabels([f'{t / ms(seconds=1):g}' for t in ticks[:-1]] + [f'≥{clip_at / ms(seconds=1):g}'])
    ax.set(ylim=(0, clip_at), ylabel='seconds')

    ax = axs[-1]

    splot.drop_spines_grid(axs)

    splot.set_time_ticks(ax, scale='minutes', major=ms(minutes=15), minor=ms(minutes=5))
    ax.spines['bottom'].set_position(('outward', 4))

    return f


def collect_lengths(rem_wins, analysis_wins):
    lengths = []

    for i, (idx, win) in enumerate(analysis_wins.iter_wins()):
        #         wins = rem_wins.crop_to_main(win)
        wins = rem_wins.sel_mask(rem_wins.is_within(win))
        wins['length'] = wins.lengths()
        wins['when'] = analysis_wins.loc[idx, 'cat']
        #         wins['original_train'] = analysis_wins.loc[idx, 'original_train']

        lengths.append(
            wins[['when', 'cat', 'length']]
        )

    lengths = pd.concat(lengths, ignore_index=True)

    return lengths


def combine_consecutive_pairs(rem_wins):
    wins = pd.DataFrame({
        'start': rem_wins['start'],
        'stop': rem_wins['stop'].shift(-1),
        'cat': rem_wins['cat'] + '-' + rem_wins['cat'].shift(-1),
    })

    return timeslice.Windows(wins)


def plot_lengths_single(ax, lengths, color='k', is_time=True):
    when_values = lengths['when'].unique()

    grouped = lengths.groupby('when')['length']

    summaries = grouped.mean()

    for j, when in enumerate(when_values):
        values = grouped.get_group(when)

        x_noise = np.clip(np.random.normal(size=len(values), loc=0, scale=0.05), -.1, .1)

        ax.scatter(
            np.zeros(len(values)) + j + x_noise,
            values,
            facecolor=color,
            clip_on=False,
            alpha=.25,
            # edgecolor='k',
            # linewidth=0.25,
            s=100,
        )

        ax.text(
            j,
            0,
            f'n={len(values)}',
            transform=ax.get_xaxis_transform(),
            fontsize=4,
            va='bottom',
            ha='center',

        )

    for j, when in enumerate(when_values[1:]):

        v0 = grouped.get_group(when_values[0])
        v1 = grouped.get_group(when)

        u, p = scipy.stats.mannwhitneyu(v0, v1, alternative='two-sided')

        x = [0, j + 1]
        y = [1 - .05 - .05 * j] * 2
        ax.plot(
            x,
            y,
            transform=ax.get_xaxis_transform(),
            color='k',
            clip_on=False,
            linewidth=.5,
        )

        diff = (summaries[when] - summaries[when_values[0]])

        desc = ''

        if is_time:
            desc += f'{diff:+.0f}s '

        desc += f'{splot.p_value_stars(p)}'

        desc += f' (U={u}; p={p:.4f})'

        ax.text(
            np.min(x),
            np.mean(y),
            #             desc,
            desc.replace('\n', '  '),
            transform=ax.get_xaxis_transform(),
            ha='left',
            va='bottom',
            fontsize=4,
            clip_on=False,
        )

    ax.plot(
        np.arange(len(when_values)),
        summaries.loc[list(when_values)],
        '_-',
        color='xkcd:cyan',
        linewidth=.5,
        markersize=10,

    )


def plot_lengths(lengths):
    when_values = lengths['when'].unique()

    f, axs = plt.subplots(ncols=2, figsize=(5, 2))

    for k, cat in enumerate(['rem', 'sws']):
        ax = axs[k]

        plot_lengths_single(
            ax,
            lengths[lengths['cat'] == cat],
            color='k',
        )

        ax.set(title=cat.upper())

    for ax in axs.ravel()[:-1]:
        splot.set_time_ticks(ax, which='y', lim=(ms(minutes=0), ms(minutes=3)), scale='seconds')

    for ax in axs.ravel():
        ax.set_xticks(np.arange(len(when_values)))
        ax.set_xticklabels([v.replace('+', '\n+') for v in when_values])
        ax.tick_params(labelleft=True, labelbottom=True, bottom=False)
        ax.set(xlim=(-.5, len(when_values) - .5))

    return f
