"""
Additional revision questions.
"""
import logging

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import events as ev
from ihrem import plot as splot
from ihrem import stacks, timeslice
from ihrem.analysis import sleep
from ihrem.timeslice import MS_TO_S, ms


def collect_loaders(reg_sel):
    # There is an off-by-one issue with loaders at the moment
    # due to some probes start counting their filenames and metadata by 0 or 1
    try:

        reg_sel['ch0'] += 1
        reg_sel['ch1'] += 1

        loaders = {
            exp_name: reg_sel.get_loader_simplified(exp_name, area='CLA')
            for exp_name in pbar(reg_sel.experiment_names)
        }

    finally:
        reg_sel['ch0'] -= 1
        reg_sel['ch1'] -= 1

    return loaders


def get_all_exp_swr_detected(reg_sel, exp_rem_wins):
    exp_swr_detected = {}

    for exp_name in pbar(exp_rem_wins.keys()):

        swr_detected = ev.Events.from_hdf(
            reg_sel.get_path(exp_name) / f'swsort/swr_full.h5',
            'swr',
        )

        rem_wins = exp_rem_wins[exp_name]

        swr_detected[['win_idx', 'cat', 'delay']] = (
            rem_wins.classify_events(swr_detected['ref_time'])[['win_idx', 'cat', 'delay']]
        )
        swr_detected['exp_name'] = exp_name

        exp_swr_detected[exp_name] = swr_detected

        counts = swr_detected.reg.groupby(['win_idx', 'channel']).size().unstack('channel')

        rates = (counts.T / (rem_wins.lengths() * MS_TO_S)).T

        counts = counts.add_prefix('swr_count_ch')
        rates = rates.add_prefix('swr_rate_ch')

        for col, vals in counts.items():
            rem_wins[col] = vals

        for col, vals in rates.items():
            rem_wins[col] = vals

    return exp_swr_detected


def extract_swr(loader, reg, exp_name, process_win=None, overwrite=False, suffix=''):
    path = reg.get_path(exp_name) / f'swsort/swr{suffix}.h5'

    if path.exists() and not overwrite:
        print(f'Skipping {exp_name}')
        return

    main = stacks.Stack.load_single_ms(loader, process_win)

    swr = sleep.find_sharp_waves(
        main,
        low_pass_hz=30,
        downsample_hz=1000,
        width_ms=150,
        height=1,
        prominence=1,
        negative=True,
    )

    swr = swr.rename(columns=dict(time='ref_time'))

    swr = ev.Events(swr)

    print(f'Saving: {path}')

    swr.to_hdf(str(path), 'swr')


def extract_swr_multi(reg_sel, loaders):
    for exp_name in pbar(reg_sel.experiment_names):
        extract_swr(
            loaders[exp_name],
            reg_sel,
            exp_name,
            overwrite=False,
            process_win=None,
            suffix='_full',
        )


def collect_rates(exp_rem_wins, ch, bin_size, valid_win):
    rates = pd.DataFrame({
        exp_name: rem_wins[f'swr_rate_ch{ch}'].groupby(
            pd.cut(rem_wins['ref'], bins=np.arange(*valid_win, bin_size))).mean()
        for exp_name, rem_wins in exp_rem_wins.items()
    })

    rates.index = pd.IntervalIndex(rates.index)

    return rates


def collect_rates_smooth(exp_rem_wins, valid_win, smoothing_win=ms(minutes=30)):
    bin_size = ms(minutes=.1)

    rates = pd.concat({
        'ch0': collect_rates(exp_rem_wins, ch=0, bin_size=ms(minutes=.1), valid_win=valid_win),
        'ch1': collect_rates(exp_rem_wins, ch=1, bin_size=ms(minutes=.1), valid_win=valid_win),
    }, axis=1)

    rates.index = rates.index.mid

    rates = rates.interpolate().rolling(int(smoothing_win / bin_size), center=True).mean()

    return rates


def plot_rates(rates, valid_win):
    f, axs = plt.subplots(nrows=2, sharex='all', figsize=(3, 3))

    ax = axs[0]
    ax.plot(
        rates,
        linewidth=.5,
        alpha=0.25,
        clip_on=False,
        color='k',
    )
    ax.plot(
        rates.mean(axis=1),
        linewidth=1,
        alpha=1,
        clip_on=False,
        color='xkcd:magenta',
        zorder=1e6,
    )

    splot.add_desc(ax, f'n={len(rates.columns):,g}')

    ax.set(
        ylim=(0, 1),
        ylabel='Hz',

    )

    rates_z = (rates - rates.mean())

    ax = axs[1]
    ax.plot(
        rates_z,
        linewidth=.5,
        alpha=0.25,
        clip_on=False,
        color='k',
    )

    ax.plot(
        rates_z.mean(axis=1),
        clip_on=False,
        color='xkcd:magenta',
        zorder=1e6,
    )

    ax.set(
        ylim=(-.11, +.11),
        ylabel='Hz',
    )

    splot.add_desc(ax, 'mean subtracted')
    ax.spines['left'].set_bounds([-.1, +.1])

    f.suptitle(f'Mean rate of SWR within SWS\nin 30 min sliding window')

    ax.axhline(0, color='k')
    ax.spines['bottom'].set_visible(False)

    splot.drop_spines_grid(axs)

    splot.set_time_ticks(ax, scale='hours', lim=valid_win)

    return f


def collect_early_late_sws_counts(exp_swr_detected, exp_rem_wins, min_length, analysis_length):
    all_early_late = []

    for exp_name, rem_wins in pbar(exp_rem_wins.items()):

        rem_wins = rem_wins.sel(cat='sws')
        rem_wins = rem_wins.sel_length_between(min_length, np.inf)

        early_late = rem_wins.before_after(
            pre=(0, analysis_length),
            post=(-analysis_length, 0),
        )
        assert early_late.are_exclusive()

        early_late = timeslice.Windows(early_late[['original_win_idx', 'start', 'stop', 'ref', 'when']])
        early_late['exp_name'] = exp_name

        for ch in 0, 1:
            swrs = exp_swr_detected[exp_name].sel(channel=ch)

            which = early_late.classify_events(swrs['ref_time'], merge_wincols=['when'])['win_idx']

            counts = which.value_counts().reindex(early_late.index, fill_value=0)

            early_late[f'count_ch{ch}'] = counts

        all_early_late.append(early_late)

    all_early_late = timeslice.Windows.concat_list(all_early_late)

    return all_early_late


def get_early_late_sws_stats(all_early_late, analysis_length):
    means = all_early_late.groupby(['when', 'exp_name'])[['count_ch0', 'count_ch1']].mean().unstack('when').stack(0)
    stds = all_early_late.groupby(['when', 'exp_name'])[['count_ch0', 'count_ch1']].std().unstack('when').stack(0)

    means = means / (analysis_length * MS_TO_S)
    stds = stds / (analysis_length * MS_TO_S)

    cols = ['pre', 'post']

    means = means[cols]
    stds = stds[cols]

    valid = (means != 0).all(axis=1)

    if np.any(~valid):
        logging.warning(f'Bad SWR detection? {means.index[~valid].values}')

    means = means[valid]
    stds = stds[valid]

    return means, stds


def compare_early_late(means, stds, min_length, analysis_length):
    cols = means.columns

    f, ax = plt.subplots(figsize=(2, 2))

    x_noise = np.random.normal(size=len(means), scale=0.025).clip(-.1, +.1)

    for i, col in enumerate(cols):
        x = np.zeros(len(means)) + i + x_noise

        color = '#272727' if i == 0 else '#2BBAF2'

        ax.scatter(
            x,
            means[col],
            clip_on=False,
            facecolor=color,
            edgecolor='none',
            s=100,
            alpha=0.5,
            zorder=1e5,
        )

        ax.plot(
            [x] * 2,
            [
                means[col] - stds[col],
                means[col] + stds[col],
            ],
            clip_on=False,
            color=color,
            linewidth=1,
            alpha=0.5,
        )

    ax.plot(
        np.array([np.zeros(len(means)), np.ones(len(means))]) + x_noise,
        means[cols].T,
        clip_on=False,
        linewidth=0.25,
        color='#272727',
    )

    ax.plot(
        [0, 1],
        means[cols].mean(),
        '_-',
        zorder=1e6,
        color='xkcd:magenta',
        linewidth=0.5,
        markersize=10,

    )

    ax.set(
        xticks=[0, 1],
        xticklabels=['early', 'late'],
        ylim=(0, 1),
        xlim=(-.5, 1.5),
        ylabel='Hz',
        title=f'SWR rate in first and last {analysis_length * MS_TO_S:g}s of SWS\n(mean±std per recording & channel)\n(SWS longer than {min_length * MS_TO_S:g}s)\n',
    )

    ax.spines['bottom'].set_position(('outward', 4))

    splot.wilcoxon_test(
        ax,
        means['pre'], means['post'],
        effect_size=True,
    )

    return f
