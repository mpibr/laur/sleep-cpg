"""
Further characterization of the ultradian rhythm in Pogona
"""
import logging
from datetime import datetime

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import timeslice
from ihrem import traces as tr
from ihrem.timeslice import ms


def string_to_datetime(date_string):
    format_string = "%Y-%m-%d_%H-%M-%S"
    return datetime.strptime(date_string, format_string)


def collect_lights_onoff(exp_light_wins, isolation=ms(hours=3)):
    exp_lights_onoff = {}

    for exp_name, light_wins in exp_light_wins.items():
        win_idx = 0
        if (light_wins.iloc[win_idx]['cat'] == 'on') & (~light_wins.iloc[win_idx]['is_pulse']):
            time = light_wins.iloc[win_idx]['stop']

            next_light = light_wins.sel_between(start=(time, np.inf)).sel(cat='on')['start'].min() - time

            if next_light >= isolation:
                exp_lights_onoff[exp_name, 'off'] = time

        win_idx = -1
        if (light_wins.iloc[win_idx]['cat'] == 'on') & (~light_wins.iloc[win_idx]['is_pulse']):
            time = light_wins.iloc[win_idx]['start']

            prev_light = time - light_wins.sel_between(stop=(-np.inf, time)).sel(cat='on')['stop'].max()

            if prev_light >= isolation:
                exp_lights_onoff[exp_name, 'on'] = time

    exp_lights_onoff = pd.Series(exp_lights_onoff)
    exp_lights_onoff = exp_lights_onoff.unstack().rename_axis(index='exp_name', columns='lights')

    exp_lights_onoff.dropna(inplace=True)

    assert np.all(exp_lights_onoff['on'] > exp_lights_onoff['off'])

    return exp_lights_onoff


def convert_to_solar_multi(reg_full, exp_lights_onoff):
    result = {}

    for exp_name, values in pbar(exp_lights_onoff.T.items(), total=exp_lights_onoff.shape[0]):

        full_path = reg_full.get_path(exp_name)
        try:
            first_timestamp = string_to_datetime(full_path.name)

            result[exp_name] = timeslice.time_to_solar(values, first_timestamp)

        except ValueError:
            logging.exception(f'{exp_name}: {full_path}')

    result = pd.DataFrame.from_dict(result, orient='index')
    result = result.rename_axis(index=exp_lights_onoff.index.name, columns=exp_lights_onoff.columns.name)

    return result


def plot_median_beta_racorr(racorr_cut_off, racorr_cut_on):
    inter_quantile_range = (.05, .95)

    f, axs = plt.subplots(nrows=2, ncols=2, sharex='col', sharey='row', figsize=(4, 3))

    for j, (desc, cut) in enumerate([('off', racorr_cut_off), ('on', racorr_cut_on)]):

        for i, which in enumerate(['lag', 'corr']):

            ax = axs[i, j]

            traces = cut.sel(which=which)

            q0 = traces.quantile(inter_quantile_range[0], axis=1)
            q1 = traces.quantile(inter_quantile_range[1], axis=1)

            summary = traces.median(axis=1)

            ax.fill_between(
                q0.index,
                q0.values,
                q1.values,
                facecolor='xkcd:silver',
                alpha=.5,
            )

            ax.plot(
                summary,
                color='k',
                linewidth=1,
            )

            if which == 'lag':
                splot.set_time_ticks(ax, which='y', scale='seconds', major=ms(seconds=20),
                                     lim=(ms(minutes=1), ms(minutes=3.5)))

                time_win = traces.get_rel_win()

                if desc == 'off':
                    splot.plot_light_protocol_bar(
                        ax,
                        timeslice.Windows.build_from_dict({
                            'on': (time_win.start, 0),
                            'off': (0, time_win.stop),
                        })
                    )
                else:

                    splot.plot_light_protocol_bar(
                        ax,
                        timeslice.Windows.build_from_dict({
                            'off': (time_win.start, 0),
                            'on': (0, time_win.stop),
                        })
                    )
            else:
                ax.set(ylim=(0, 1))

            ax.axvline(0, color='xkcd:cyan', linestyle='--')

    splot.drop_spines_grid(axs)

    axs[0, 0].set(ylabel='lag (sec)')
    axs[1, 0].set(ylabel='corr')

    count = len(racorr_cut_off.sel(which='corr').index)

    f.suptitle(f'beta lagged auto-corr\n median [{inter_quantile_range[0]}-{inter_quantile_range[1]}] (n={count})')

    for ax in axs[-1, :]:
        splot.set_time_ticks(ax, scale='hours', major=ms(hours=1), tight=False)

    return f


def take_rcorr_trace(rcorr):
    rcorr = rcorr.copy()

    rcorr[rcorr < 0] = np.nan

    best_lag = rcorr.idxmax(axis=1)

    best_lag[best_lag == rcorr.columns.min()] = np.nan
    best_lag[best_lag == rcorr.columns.max()] = np.nan

    best_lag = best_lag.dropna()

    best_rcorr = pd.Series({time: rcorr.loc[time, lag] for time, lag in best_lag.items()})

    return pd.DataFrame({
        'corr': best_rcorr,
        'lag': best_lag,
    })


def take_rcorr_trace_multi(beta_rcorr):
    traces = {
        exp_name: take_rcorr_trace(rcorr)
        for exp_name, rcorr in beta_rcorr.items()
    }

    traces = pd.concat(traces, names=['exp_name', 'which'], axis=1)

    return tr.Traces.from_df(traces)
