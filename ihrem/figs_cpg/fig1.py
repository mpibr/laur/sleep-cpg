"""
Pogona’s regular ultradian sleep rhythm is reset by single pulses of light.
"""
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import timeslice
from ihrem.analysis import stim
from ihrem.timeslice import ms, Win

BETA_XCORR_DEFAULT = dict(
    lags_ms=np.arange(ms(minutes=-4), ms(minutes=+4) + 1, ms(seconds=1)),
    sliding_win_ms=ms(minutes=20),
    sliding_step_ms=ms(minutes=1),
)


def savefig(f, name):
    splot.savefig(f, name=f'fig1/{name}')


def plot_24h(exp_beta_detailed, first_timestamp, time_win=None, light_wins=None):
    assert exp_beta_detailed['exp_name'].nunique() == 1

    if time_win is None:
        time_win = exp_beta_detailed.get_rel_win().round(scale='hours')

    traces = exp_beta_detailed.crop(time_win)

    axs_dict = splot.plot_traces_wrapped(
        traces.get_df('ch'), figsize=(9, 4), ylim=(-.01, 1.5),
        tbin_width=ms(minutes=180),
        tstart_timestamp=first_timestamp,
        wins=light_wins,
        suptitle=exp_beta_detailed['exp_name'].iloc[0],
    )

    ax = list(axs_dict.values())[0]
    splot.add_yscale_bar(ax)

    return ax.figure


def plot_24h_beta_racorr(exp_beta_detailed, first_timestamp, light_wins=None):
    assert exp_beta_detailed['exp_name'].nunique() == 1

    exp_beta_comb = exp_beta_detailed.groupby_max('exp_name')
    # exp_beta_comb = exp_beta_detailed.sel(ch='ch1')

    beta_racorr = exp_beta_comb.auto_corr_rolling(**BETA_XCORR_DEFAULT, pbar=pbar)

    assert len(beta_racorr) == 1
    beta_racorr = list(beta_racorr.values())[0]
    beta_racorr.index = timeslice.time_to_solar(beta_racorr.index, first_timestamp)

    f, ax = plt.subplots(figsize=(6, 2))

    splot.plot_racorr(ax, beta_racorr, yscale='minutes', xscale='hours')
    splot.set_time_ticks(ax, scale='hours', minor=ms(hours=1), major=ms(hours=2))
    splot.set_ticks_solar_time(ax)
    f.suptitle(exp_beta_detailed['exp_name'].iloc[0])

    splot.plot_light_protocol_bar(ax, light_wins.shift(timeslice.time_to_solar(0, first_timestamp)))

    ax.set(
        xlim=timeslice.time_to_solar(np.array(exp_beta_detailed.get_rel_win()), first_timestamp),
    )

    return beta_racorr, f


def detect_first_rem_post_pulse(exp_rem_wins, analysis_windows, min_delay=ms(seconds=30)):
    first_rem_time = {}

    for k, win, props in analysis_windows.iter_wins_items(show_pbar=True):
        rem_wins = exp_rem_wins[props['exp_name']]

        rem_wins = rem_wins.crop_to_main(win).shift(-props['ref'])

        first_rem_time[k] = rem_wins.sel_between(start=(min_delay, np.inf)).sel(cat='rem')['start'].min()

    return pd.Series(first_rem_time)


def plot_examples_multiple_nights(beta_cut, exp_names, figsize=(3, 5), summary_func=pd.DataFrame.mean):
    beta_cut = beta_cut.sel_isin(exp_name=exp_names)

    f, axs = plt.subplots(
        figsize=figsize,
        nrows=2,
        sharex='all',
        gridspec_kw=dict(height_ratios=[9, 1])
    )

    ax = axs[0]

    y_offset = 0

    y_step = -1

    for exp_name in exp_names:
        traces = beta_cut.sel(exp_name=exp_name)

        y_offset_start = y_offset

        pulse_win = traces[['pulse_start', 'pulse_stop']]

        pulse_win = (pulse_win.T - traces['ref'].T).T

        pulse_win = Win(pulse_win['pulse_start'].min(), pulse_win['pulse_stop'].max())

        ax.text(
            0, y_offset - y_step,
            f'{exp_name} ({pulse_win.length / ms(seconds=1):,g}s)',
            fontsize=5,
        )

        for _, trace in traces.traces.items():
            ax.fill_between(
                trace.index,
                np.zeros(len(trace)) + y_offset,
                trace.values + y_offset,
                facecolor='k',
                clip_on=False,
                zorder=1e4,
            )

            y_offset += y_step

        ax.fill_between(
            pulse_win,
            [y_offset_start - y_step] * 2,
            [y_offset - y_step] * 2,
            facecolor='#F9DB00',
        )

        y_offset += y_step * 2

    splot.drop_spine(ax, 'y')
    splot.drop_spine(ax, 'x')
    splot.add_yscale_bar(ax)

    ax.set(ylim=(y_offset - y_step * 2, -y_step * 2))

    ax = axs[1]

    for k, trace in beta_cut.traces.items():
        ax.plot(trace, color='xkcd:silver', linewidth=0.25, alpha=.5)

    ax.plot(summary_func(beta_cut.traces, axis=1), color='k', linewidth=1)
    splot.set_time_ticks(ax, scale='minutes', lim=beta_cut.get_rel_win(), label='Time lag (min)')
    splot.add_yscale_bar(ax)

    splot.add_desc(
        ax,
        f'n={len(beta_cut.index):,g}',
        loc='upper right',
        bkg_color='none',
    )

    for ax in axs:
        ax.axvline(0, linestyle='--', color=splot.COLORS['pulse'], linewidth=1, zorder=1e6)

    return f


def collect_len_stats(exp_rem_wins, max_cycle_length=ms(minutes=4), focus_win=(ms(hours=2), ms(hours=10))):
    all_len_stats = {}

    for exp_name, rem_wins in exp_rem_wins.items():
        rem_wins['length'] = rem_wins.lengths()

        assert (
                np.all(rem_wins['cat'] != rem_wins['cat'].shift())
                &
                np.all(rem_wins['cat'].iloc[2:] == rem_wins['cat'].shift().shift().dropna())
        ), 'cats should be alternating'

        length_plus_next = rem_wins['length'] + rem_wins['length'].shift(-1)

        # to stay consistent, let's say cycle starts with SWS
        cycle_length = length_plus_next[rem_wins['cat'] == 'sws'].reindex(rem_wins.index).ffill()

        rem_wins['cycle_length'] = cycle_length

        rem_wins = rem_wins.sel_between(cycle_length=(0, max_cycle_length))

        rem_wins['duty'] = rem_wins['length'] / rem_wins['cycle_length']

        rem_wins_inside = rem_wins.sel_between(ref=focus_win)
        rem_wins_cropped = rem_wins.crop_to_main(focus_win)

        all_len_stats[exp_name] = pd.Series({
            'rem_median': rem_wins_inside.sel(cat='rem')['length'].median(),
            'sws_median': rem_wins_inside.sel(cat='sws')['length'].median(),
            'cycle_median': rem_wins_inside.sel(cat='sws')['cycle_length'].median(),
            'duty_median': rem_wins_inside.sel(cat='sws')['duty'].median(),

            'cycle_count': rem_wins_cropped.sel(cat='sws')['length'].count(),
            'rem_total': rem_wins_cropped.sel(cat='rem')['length'].sum(),
            'sws_total': rem_wins_cropped.sel(cat='sws')['length'].sum(),
        })

    all_len_stats = pd.DataFrame.from_dict(all_len_stats, orient='index')

    return all_len_stats


def report_noisy(reg_full):
    used_durations = [
        10,
        500,
        1_000,
        5_000,
        30_000,
        45_000,
        60_000,
        90_000,

    ]

    all_pulses = stim.collect_analysis_windows(reg_full, pulse_len=None, quiet=True)

    all_used_pulses = all_pulses.sel_mask(
        (all_pulses['pulse_len'].isin(used_durations) & all_pulses['cap'].isna())
        |
        (all_pulses['pulse_len'].isin([1_000]))
    )

    noisy_counts = all_used_pulses['noisy'].value_counts()

    print(
        f'{noisy_counts[True]}/{noisy_counts.sum()} pulses labeled as noisy '
        f'({noisy_counts[True] / noisy_counts.sum() * 100:.2f}%)'
    )

    summary = all_used_pulses.wins.fillna('none').value_counts([
        'stim', 'state', 'lesion', 'pulse_len', 'cap', 'noisy'
    ]).unstack('noisy', fill_value=0).sort_index()

    return summary


def print_reset_stats(analysis_windows, desc='', col='first_rem_time_s'):
    mean = analysis_windows[col].mean()
    std = analysis_windows[col].std()

    median = analysis_windows[col].median()
    q0 = analysis_windows[col].quantile(.25)
    q1 = analysis_windows[col].quantile(.75)

    print(
        f'{desc} mean±std: {mean: 3.0f}±{std: 3.0f} \t'
        f'median: {median:.0f}s; '
        f'[25th, 75th] percentiles: [{q0:.0f}, {q1:.0f}]s; '
        f'n={len(analysis_windows)}'
    )


def plot_rem_prob(rem_traces, when=0):
    f, ax = plt.subplots()

    ax.fill_between(
        (- rem_traces['pulse_len'].mean(), 0),
        [0, 0],
        [1, 1],
        facecolor='#F9DB00',
        transform=ax.get_xaxis_transform(),
    )

    is_rem = rem_traces.applymap({'rem': 1, 'sws': 0})

    prob = is_rem.mean(axis=1)

    ax.plot(
        prob,
        color='k',
    )

    ax.scatter(
        [when],
        [prob.loc[when]],
        facecolor='xkcd:magenta',
        s=100,
        edgecolor='w',
        linewidth=.5,
        zorder=1e7,
    )

    splot.set_time_ticks(
        ax,
        scale='minutes',
    )

    ax.set_ylim(0, 1)
    ax.spines['left'].set_position(('outward', 2))
    ax.spines['bottom'].set_position(('outward', 2))

    count = np.count_nonzero(is_rem.tloc[when])
    total = len(is_rem.index)

    splot.add_desc(
        ax,
        f'{count / total * 100:.1f}% chance of being at rem at t={when} ({count}/{total})',
        loc='bottom right',
        color='xkcd:magenta',
    )
