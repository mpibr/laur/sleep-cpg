"""
A sleep-like rhythm can be entrained in awake animals by alternating light and dark at a rate matching REM and NREM alternation during sleep
"""
import matplotlib.gridspec
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import events as ev
from ihrem import plot as splot
from ihrem import traces as tr
from ihrem import trains, timeslice, stacks
from ihrem.analysis import sleep
from ihrem.analysis import sne
from ihrem.timeslice import ms, Win, MS_TO_S


def savefig(f, name):
    splot.savefig(f, name=f'fig3/{name}')


def get_protocol_colors(all_trains):
    color_cycle = [
        '#1f77b4',  # blue
        '#ff7f0e',  # orange
        '#2ca02c',  # green
        '#d62728',  # red
        '#9467bd',  # purple
        '#8c564b',  # brown
        '#e377c2',  # pink
        '#7f7f7f',  # gray
        '#bcbd22',  # olive
        '#17becf',  # cyan
        '#aec7e8',  # light blue
        '#ffbb78',  # light orange
        '#98df8a',  # light green
        '#ff9896',  # light red
        '#c5b0d5',  # light purple
    ]

    protocol_colors = {
        tuple(k): color_cycle[i % len(color_cycle)]
        for i, (_, *k) in enumerate(all_trains[['pulse_len', 'interval', 'dark_after']].drop_duplicates().itertuples())
    }

    return protocol_colors


def plot_summary_spread(traces, light_wins, protocol_colors):
    f, ax = plt.subplots(figsize=(9, 5))

    yscale = .9

    traces = traces.sort_values([
        'interval', 'pulse_len', 'dark_after', 'pulse_count'
    ], ascending=False)

    for i, k in enumerate(pbar(traces.index)):
        offset = len(traces.index) - i - 1

        trace = traces.traces[k]

        ax.fill_between(
            trace.index,
            np.zeros_like(trace.index) + offset,
            trace.values * yscale + offset,
            edgecolor='none',
            facecolor='k',
            zorder=1e5,
        )

        splot.plot_wins_fill(
            ax,
            light_wins[k],
            transform=ax.transData,
            ymin=offset,
            ymax=offset + yscale,
            window_colors=dict(on='w', off='xkcd:silver')
        )

        interval = traces.loc[k, 'interval']
        pulse_len = traces.loc[k, 'pulse_len']
        pulse_count = traces.loc[k, 'pulse_count']
        dark_after = traces.loc[k, 'dark_after']

        ax.plot(
            [interval, pulse_len + interval],
            [offset, offset],
            color='xkcd:magenta',
            linewidth=1,
            zorder=1e7,
            alpha=1,
            solid_capstyle='butt'
        )

        desc = f'{pulse_count}p {pulse_len / 1000:g}off-{interval / 1000:g}on'

        ax.text(
            0,
            offset + yscale * .5,
            traces.loc[k, 'exp_name'] + ' ' + desc,
            va='center',
            ha='left',
            fontsize=4,
            transform=ax.get_yaxis_transform(),
            bbox=dict(facecolor='w', alpha=.95, edgecolor='none'),
            zorder=1e6,
            color=protocol_colors[pulse_len, interval, dark_after],
        )

    ax.tick_params(left=False, labelleft=False)
    ax.spines['left'].set_visible(False)

    splot.set_time_ticks(ax, scale='minutes', major=ms(minutes=5), minor=ms(minutes=1))

    return f


def plot_summary_overlaid_by_protocol(beta_aligned, light_wins_aligned, protocol_colors):
    grouped = list(beta_aligned.iter_grouped(['pulse_len', 'interval', 'dark_after']))

    f, axs = plt.subplots(nrows=len(grouped), sharex='all', sharey='all', figsize=(6, 3))

    for i, ((pulse_len, interval, dark_after), traces) in enumerate(grouped):
        color = protocol_colors[pulse_len, interval, dark_after]

        ax = axs[i]

        traces = beta_aligned.sel(pulse_len=pulse_len, interval=interval, dark_after=dark_after)
        traces = traces.downsample(1000).clip(upper=1.5)

        min_common = -(pulse_len + interval) * traces['pulse_count'].min()
        valid_win = (max(min_common, ms(minutes=-20)), ms(minutes=10))
        traces = traces.crop(valid_win).dropna()

        light_wins = light_wins_aligned[traces.index[0]].crop_to_main(valid_win)

        desc = f'{pulse_len / 1000:g}off-{interval / 1000:g}on n={len(traces.index)}'

        splot.add_desc(ax, desc, loc='center left', color=color, x=0, bkg_alpha=.9)

        if len(traces.index) > 1:
            ax.plot(
                traces.time,
                traces.traces.values,
                linewidth=.5,
                color='xkcd:silver',
                alpha=.75,
            )

        ax.plot(
            traces.time,
            traces.median(axis=1).values,
            linewidth=1,
            color='k',
        )

        splot.plot_wins_rectangle(
            ax,
            light_wins,
            y0=-.1,
            y1=-.3,
            clip_on=False,
            transform=ax.transData,
            colors=dict(on='w', off='grey'),
            how='face',
            edgecolor='k',
            linewidth=0.25
        )

        ax.plot(
            [interval, pulse_len + interval],
            [-.2, -.2],
            color='xkcd:magenta',
            linewidth=1,
            zorder=1e7,
            alpha=1,
            solid_capstyle='butt',
            clip_on=False,
        )
        ax.axvline(0, color='k', linestyle='--', linewidth=0.25)
        ax.axvline(ms(minutes=1), color='k', linestyle='--', linewidth=0.25)

    splot.set_time_ticks(axs[-1], scale='minutes', major=ms(minutes=1))

    splot.drop_spines_grid(axs, left_edge=True)
    splot.add_yscale_bar(axs[-1])

    axs[-1].spines['bottom'].set_position(('outward', 2))

    return f


def load_spiking_multi(exp_paths, reg_sel):
    exp_tracking = {}

    for exp_name, tracking_path in pbar(exp_paths.items(), desc='load', total=len(exp_paths)):
        exp_tracking[exp_name] = trains.SpikeTrains.load_spikes_jrclust(
            tracking_path,
            win_ms=reg_sel.get_loader(exp_name, accept_non_interp=True).win_ms,
        )

    return exp_tracking


def detect_protocol_end(exp_light_wins, all_trains):
    last_on = {}
    dark_after = {}

    for k, win, props in all_trains.iter_wins_items():
        light_wins = exp_light_wins[props['exp_name']]

        train_lights = light_wins.sel(train=props['local_train'])

        train_lights = train_lights.sel(cat='on')

        last_on[k] = train_lights.sel_mask(train_lights.lengths().between(ms(minutes=.5), ms(minutes=3)))['stop'].max()

        dark_after[k] = len(
            light_wins.crop_to_main((last_on[k], last_on[k] + ms(minutes=5))).drop_empty().sel(cat='on')) == 0

    all_trains['last_on'] = pd.Series(last_on)
    all_trains['dark_after'] = pd.Series(dark_after)


def plot_single_example(exp_beta_detailed, exp_light_wins, exp_name, ch):
    light_wins = exp_light_wins[exp_name]
    trace = exp_beta_detailed.sel(exp_name=exp_name, ch=f'ch{ch}').get()

    lights_off = light_wins.sel(cat='off')['start'].min()

    zoom_win = Win(lights_off - ms(seconds=30), lights_off + ms(minutes=2))

    trace = zoom_win.crop_df(trace)
    light_wins = light_wins.crop_to_main(zoom_win)

    f, ax = plt.subplots()

    ax.fill_between(
        trace.index,
        np.zeros(len(trace)) + trace.min(),
        trace.values,
        facecolor=splot.COLORS[f'ch{ch}']
    )

    splot.plot_wins_fill(ax, light_wins)
    splot.set_time_ticks(ax, scale='seconds', major=ms(seconds=20))
    splot.add_yscale_bar(ax, vmax=0.5, desc=True, unit='(A.U.)')

    f.suptitle(exp_name)

    return f


def align_windows_dict(exp_light_wins, align_wins):
    return {
        k: exp_light_wins[props['exp_name']].crop_to_main(zoom_win).shift(-props['ref'])
        for k, zoom_win, props in align_wins.iter_wins_items()
    }


def report_reset_times(beta_aligned, pulse_thresh):
    for pulse_len, thresh in pulse_thresh:

        sel = beta_aligned.sel(dark_after=True).sel_between(pulse_count=(4, np.inf)).sort_values(
            ['interval', 'pulse_len', 'pulse_count'], ascending=False).sel(interval=pulse_len,
                                                                           pulse_len=pulse_len).crop(
            (ms(minutes=-20), ms(minutes=6)))

        sel = sel.normalize_by_quantiles()

        # exp_rem_wins = cycles.extract_rem_wins_multi(sel, key=None)

        exp_rem_wins = {
            k: timeslice.Windows.build_from_contiguous_values(
                (trace > thresh).map({True: 'rem', False: 'sws'})).merge_sandwiched(max_length=ms(seconds=5))
            for k, trace in sel.traces.items()
        }

        first_rem = {}

        for i, (k, trace) in enumerate(sel.traces.items()):
            wins = exp_rem_wins[k]

            first_rem[k] = wins.sel_between(start=(0, np.inf)).sel(cat='rem')['start'].min()

        first_rem = pd.Series(first_rem)

        print(
            f'for {pulse_len / (ms(seconds=1)):g}s, median: {first_rem.median() / (ms(seconds=1)):.1f}; IQR: [{first_rem.quantile(.25) / (ms(seconds=1)):.1f}, {first_rem.quantile(.75) / (ms(seconds=1)):.1f}]; n={len(first_rem):,g}')


def print_animal_counts(beta_aligned):
    df = pd.DataFrame({
        'animals': beta_aligned.reg.groupby(['interval'])['animal'].nunique(),
        'exp': beta_aligned.reg.groupby(['interval'])['animal'].size(),
    })

    df.index = (df.index // 1000).astype(int)
    # display(df.loc[[30, 45, 60, 80, 90]])

    for interval, animals, exp in df.itertuples():
        print(f'{interval}s: {animals}, {exp}')

    total_animals = beta_aligned['animal'].nunique()
    print(f'Total: {len(beta_aligned.index):g} ({total_animals:g})')


def load_lfp(reg_sel, exp_name, load_hz=None):
    loader = reg_sel.get_loader_simplified(exp_name)

    if load_hz is None:
        load_hz = loader.sampling_rate

    load_step = timeslice.get_stride(loader.sampling_rate, load_hz)

    lfp = loader.load(
        slice(loader.win_idcs.start, loader.win_idcs.stop, load_step),
        channels=[0, 1],
    )

    lfp = pd.DataFrame(
        lfp.T,
        columns=reg_sel.loc[exp_name, ['side0', 'side1']].values,
        index=np.arange(loader.win_idcs.start, loader.win_idcs.stop, load_step) * loader.sampling_period
    ).rename_axis(columns='side', index='time')

    lfp = tr.Traces.from_multiindex_df(lfp)

    return lfp


def plot_single_example_with_spikes(light_wins, spikes, beta, lfp, zoom_win, filter_pass, ref_time):
    fr_bin_size = 2
    fr = spikes.compute_activity_per_cluster(spikes.win_ms.arange(ms(seconds=fr_bin_size)), fr=True, show_pbar=False)
    fr.index = fr.index.mid
    fr = tr.Traces.from_df(fr)

    fr = fr.crop(zoom_win).shift_time(-ref_time)
    beta = beta.crop(zoom_win).shift_time(-ref_time)
    lfp = lfp.crop(zoom_win).shift_time(-ref_time)
    spikes = spikes.sel_between(time=zoom_win).shift_time(-ref_time)
    light_wins = light_wins.crop_to_main(zoom_win).shift(-ref_time)

    lfp = lfp.filter_pass(filter_pass)

    f, axs = plt.subplots(nrows=6, sharex='all', figsize=(5, 3))

    for ax in axs.ravel():
        splot.plot_wins_fill(ax, light_wins)

    ax = axs[0]

    ax.plot(
        fr.traces,
        color='xkcd:silver',
        linewidth=.25,
        alpha=1,
    )

    ax.plot(
        fr.mean(axis=1),
        color='k',
    )

    ax.set(
        ylabel='Hz',
        yticks=np.arange(0, 25, 5),
    )

    splot.add_desc(ax, f'n={len(fr.index):,g}  {fr_bin_size}s bins')

    ax = axs[1]

    splot.drop_spine(ax, 'y')

    ax.set(
        ylabel='spikes',
    )

    ax.scatter(
        spikes.spikes['time'],
        spikes.spikes['gid'],
        marker='|',
        color='k',
        linewidth=0.25,
        s=5,
    )

    for i, side in enumerate(['left', 'right']):
        ax = axs[2 + i]
        trace = lfp.sel(side=side).get()

        ax.plot(trace, color=splot.COLORS[side], linewidth=.5)

        splot.add_yscale_bar(ax, vmin=-500, vmax=0, desc=True)

    ax = axs[2]
    splot.add_desc(ax, splot.filter_desc(filter_pass))

    ax = axs[4]
    splot.add_yscale_bar(ax)
    ax.set(ylabel='beta')

    for i, side in enumerate(['left', 'right']):
        trace = beta.sel(side=side).get()
        ax.fill_between(
            trace.index,
            trace.values,
            facecolor=splot.COLORS[side],
            alpha=.75,
        )

    ax = axs[5]
    splot.add_yscale_bar(ax)
    ax.set(ylabel='beta')

    trace = beta.max(axis=1)
    ax.fill_between(
        trace.index,
        trace.values,
        facecolor='k',
        alpha=1,
    )

    splot.drop_spines_grid(axs)
    splot.set_time_ticks(axs[-1], scale='minutes', major=ms(minutes=1), minor=ms(seconds=15))

    return f


def plot_trace_as_im(ax, s: pd.Series, ymin, ymax, norm=None, cmap='viridis', interpolation='none', aspect='auto',
                     **kwargs):
    x_extent = s.index[[0, -1]] + np.diff(s.index)[[0, -1]] * np.array([-.5, +.5])

    ax.imshow(
        s.values.reshape(1, -1),
        extent=(*x_extent, ymin, ymax),
        aspect=aspect,
        interpolation=interpolation,
        cmap=cmap,
        norm=norm,
        origin='lower',
        **kwargs,
    )


def plot_beta_vs_eyes_barcode(eyes_aligned, beta_aligned, light_wins_aligned, thresh=.5):
    common = list(np.intersect1d(eyes_aligned['exp_name'].unique(), beta_aligned['exp_name'].unique()))
    beta_aligned = beta_aligned.sel_isin(exp_name=common)
    eyes_aligned = eyes_aligned.sel_isin(exp_name=common)

    f, axs = plt.subplots(figsize=(3, 3), nrows=3, sharex='all')

    norm = matplotlib.colors.Normalize(vmin=0, vmax=1)
    cmap = matplotlib.colormaps['binary']

    scale = .5

    protocol = beta_aligned[['interval', 'pulse_len']].drop_duplicates()
    assert len(protocol) == 1
    interval, pulse_len = protocol.iloc[0]

    f.suptitle(f'{interval * MS_TO_S:g}:{pulse_len * MS_TO_S:g}  n={len(beta_aligned.index)}')

    ax = axs[2]
    ax.plot(eyes_aligned.traces, linewidth=.5, alpha=.5, color='xkcd:grey')

    if thresh is not None:
        ax.axhline(thresh, color='xkcd:magenta', linestyle='--', linewidth=.5)

    for i, exp_name in enumerate(pbar(beta_aligned['exp_name'])):

        ax = axs[0]
        beta = beta_aligned.sel(exp_name=exp_name).get()

        ax.plot(
            beta, linewidth=.5, alpha=.5,
            color='xkcd:grey'
        )

        ax = axs[1]
        eyes = eyes_aligned.sel(exp_name=exp_name).get()

        if thresh is not None:
            eyes = eyes > thresh

        plot_trace_as_im(ax, eyes, norm=norm, cmap=cmap, ymin=i - scale * .5, ymax=i + scale * .5)

        ax.text(
            0.05, i + scale * .5, exp_name, color=f'C{i}', fontsize=4,
            va='bottom',
            transform=ax.get_yaxis_transform(),
        )

    ax = axs[0]
    ax.plot(beta_aligned.median(axis=1), color='k')

    ax = axs[2]
    ax.plot(eyes_aligned.median(axis=1), color='k')

    ax = axs[0]

    ax.plot(
        [interval, pulse_len + interval],
        [.025, .025],
        color='xkcd:magenta',
        linewidth=1,
        zorder=1e7,
        alpha=1,
        solid_capstyle='butt',
        clip_on=False,
        transform=ax.get_xaxis_transform()
    )
    splot.plot_light_protocol_bar(ax, light_wins_aligned[beta_aligned.index[0]], clip_on=True, y0=0, y1=.05,
                                  divisor=None)

    ax = axs[1]

    ax.set(ylim=(-.5, len(beta_aligned.index) - .5))

    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    cbar = f.colorbar(sm, ax=ax)
    cbar.set_label('eye')
    cbar.set_ticks([0, 1])

    splot.set_time_ticks(axs[-1], scale='minutes', lim=(ms(minutes=-4), ms(minutes=+4)))

    splot.drop_spines_grid(axs[0:2], left_edge=True, bottom_edge=True)

    if thresh is not None:
        axs[2].set_yticks([0, thresh, 1])

    #     splot.drop_spines_grid(axs, left_edge)

    ax = axs[0]
    splot.add_yscale_bar(ax)

    return f


def extract_swr(loader, reg, exp_name, process_win=None, overwrite=False):
    path = reg.get_path(exp_name) / 'swsort/swr.h5'

    if path.exists() and not overwrite:
        print(f'Skipping {exp_name}')
        return

    main = stacks.Stack.load_single_ms(loader, process_win)

    swr = sleep.find_sharp_waves(
        main,
        low_pass_hz=30,
        downsample_hz=1000,
        width_ms=150,
        height=1,
        prominence=1,
        negative=True,
    )

    swr = swr.rename(columns=dict(time='ref_time'))

    swr = ev.Events(swr)

    swr.to_hdf(str(path), 'swr')


def add_iei(sns):
    sns['iei'] = np.nan

    sns['iei'] = sns['iei'].combine_first(
        sns.get_inter_event_intervals_between_channels(first_ch=0, second_ch=0)
    )

    sns['iei'] = sns['iei'].combine_first(
        sns.get_inter_event_intervals_between_channels(first_ch=1, second_ch=1)
    )


def load_multi_sns(reg):
    exp_sns = {}

    for exp_name in pbar(reg.experiment_names):
        try:
            sns = reg.load_all_sne(exp_name, suffix='_cdf')
            sns = sns[sns['null_cdf'].between(-np.inf, 0.05)]

            sns = sne.SharpNegativeEvents(sns).patch_simplified_channels()

            add_iei(sns)

            exp_sns[exp_name] = sns

        except FileNotFoundError:
            pass

    return exp_sns


def load_multi_swr(reg):
    exp_swr = {}

    for exp_name in pbar(reg.experiment_names):
        try:
            path = reg.get_path(exp_name) / 'swsort/swr.h5'

            swr = ev.Events.from_hdf(path, 'swr')

            add_iei(swr)

            exp_swr[exp_name] = swr

        except FileNotFoundError:
            pass

    return exp_swr


def combine_all_events(exp_beta, exp_swr, exp_sns, sleep_exp, awake_exp, ref_ch):
    exp_beta_log = exp_beta.log10()

    all_events = {}

    for (state, exp_name) in ('sleep', sleep_exp), ('awake', awake_exp):

        beta_log = exp_beta_log.sel(exp_name=exp_name, probe_idx=ref_ch).get()

        for (event_type, exp_events) in ('swr', exp_swr), ('sns', exp_sns):
            evs = exp_events[exp_name].sel(channel=ref_ch)
            evs = evs.lookup_values_interp('beta_log', beta_log)

            all_events[event_type, state] = evs

    return all_events


def pick_example_events(all_events):
    high_beta_log_thresh = -.5
    low_beta_log_thresh = -2
    sample_size = 100

    all_sel = {}

    for state in 'awake', 'sleep':
        for event_type in 'sns', 'swr':

            events = all_events[event_type, state]

            beta_range = (-np.inf, low_beta_log_thresh) if event_type == 'swr' else (high_beta_log_thresh, np.inf)

            sel = events.sel_between(ref_beta_log=beta_range)

            if event_type == 'sns':
                sel = sel.sel_between(iei=(200, np.inf))

            all_sel[event_type, state] = sel.sample(min(sample_size, len(sel)))

    return all_sel


def load_event_lfp(all_sel, loaders, ref_ch):
    all_raw = {}

    for (event_type, state), sel in all_sel.items():
        raw = stacks.Stack.load_ms(
            loaders[state],
            sel['ref_time'],
            (-200, +200) if event_type == 'sns' else (-1500, +1500),
            load_hz=10_000,
            channels=[ref_ch],
        )

        raw = raw.isel(channel=0)

        all_raw[event_type, state] = raw

    return all_raw


def plot_lfp_sta_shaded(ax, raw, filter_pass=None, color='k', quantiles=(.25, .75), label=None):
    raw = raw.filter_pass(filter_pass).reset_baseline((-5, +5))

    sampling_rate = raw.estimate_sampling_rate()
    if len(raw.coords['time']) > 10_000 and sampling_rate > 1000:
        raw = raw.downsample(timeslice.match_load_hz(sampling_rate, 1000, thresh=100))

    #     ax.plot(raw.to_dataframe2d().T, linewidth=.25, alpha=.25, color='xkcd:silver')

    if quantiles is None:
        mean = raw.mean('event_id').to_series()
        std = raw.std('event_id').to_series()

        summary = mean
        bottom = mean - std
        top = mean + std

    else:
        summary = raw.median('event_id').to_series()
        bottom = raw.quantile(quantiles[0], 'event_id').to_series()
        top = raw.quantile(quantiles[1], 'event_id').to_series()

    yoff = summary.median()
    summary = summary - yoff
    bottom = bottom - yoff
    top = top - yoff

    ax.plot(
        summary,
        linewidth=1,
        color=color,
        label=f'{label} n=({raw.shape[0]:,g})',
    )

    ax.fill_between(
        summary.index,
        bottom.values,
        top.values,
        linewidth=0,
        facecolor=color,
        edgecolor='none',
        alpha=.25,
    )


def plot_lfp_sta_2x2(all_raw, quantiles=(.25, .75)):
    state_color = {
        'sleep': 'k',
        'awake': 'xkcd:cerulean',
    }

    filter_pass = (.25, 100)

    f, axs = plt.subplots(nrows=1, ncols=2, sharex='col', sharey='all', figsize=(5, 1.5),
                          gridspec_kw=dict(width_ratios=[2, 3]), squeeze=False)

    for i, state in enumerate(['awake', 'sleep']):
        for j, event_type in enumerate(['sns', 'swr']):
            ax = axs[0, j]

            raw = all_raw[event_type, state]

            plot_lfp_sta_shaded(
                ax,
                raw,
                #                 desc=f'{event_type} {state} (n={raw.shape[0]:,g})',
                filter_pass=filter_pass,
                color={
                    ('swr', 'awake'): state_color['awake'],
                    ('swr', 'sleep'): state_color['sleep'],
                    ('sns', 'awake'): state_color['awake'],
                    ('sns', 'sleep'): state_color['sleep'],
                }[event_type, state],
                quantiles=quantiles,
                label=state,
            )

    for ax in axs.ravel():
        ax.legend(fontsize=4, loc='upper left')

    splot.drop_spines_grid(axs, left_edge=True, bottom_edge=True)
    splot.add_scale_bar(axs[-1, 0], vmin=-250, vmax=0, desc=True, which='y', unit='uV')
    splot.add_scale_bar(axs[-1, 0], vmin=0, vmax=100, desc=True, which='x', unit='ms')
    splot.add_scale_bar(axs[-1, 1], vmin=0, vmax=1000, desc=True, which='x', unit='ms')

    f.suptitle((
                   f'mean+/-std' if not quantiles is not None else f'median [{quantiles[0]}, {quantiles[1]}]') + f'; {splot.filter_desc(filter_pass)}',
               fontsize=6)

    return f
