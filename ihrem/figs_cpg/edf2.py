"""
Sleep-cycle statistics calculated from the core 8h of sleep
"""
import numpy as np
import pandas as pd
import scipy.stats
from matplotlib import pyplot as plt

from ihrem import plot as splot
from ihrem.analysis import cycles, stim
from ihrem.figs_cpg import fig1
from ihrem.timeslice import ms


def is_full_night_simple_protocol(reg_full):
    exp_light_wins = stim.load_light_wins_multi(reg_full)

    all_pulses = stim.collect_all_pulses(exp_light_wins, isolation=0, pulse_len=None, length_precision=ms(minutes=1))

    simple_protocol = (
            (all_pulses['pulse_len'] == 1_000) &
            (all_pulses['to_prev'] > ms(minutes=10)) &
            (all_pulses['to_next'] > ms(minutes=10))
    )

    simple_protocol = simple_protocol.groupby(all_pulses['exp_name']).all()

    simple_protocol = simple_protocol.reindex(reg_full.index, fill_value=False)

    return simple_protocol


def collect_sleep_stats(reg_sel):
    exp_beta_detailed = reg_sel.load_all_beta_traces_multi(area='CLA').normalize_by_quantiles()

    exp_beta_comb = exp_beta_detailed.groupby_max('exp_name')

    exp_rem_wins = cycles.extract_rem_wins_multi(exp_beta_comb)

    win_size = ms(hours=2)

    all_stats = {
        'global': fig1.collect_len_stats(exp_rem_wins, focus_win=(ms(hours=2), ms(hours=10))),
        'early': fig1.collect_len_stats(exp_rem_wins, focus_win=(ms(hours=2), ms(hours=2) + win_size)),
        'late': fig1.collect_len_stats(exp_rem_wins, focus_win=(ms(hours=10) - win_size, ms(hours=10))),
    }

    summary = pd.concat(all_stats, axis=1, names=['when', 'metric'])

    return summary


def plot_dots_multi(ax, dots):
    for i, (desc, values) in enumerate(dots.items()):
        ax.scatter(
            np.zeros(len(values)) + i,
            values,
            facecolor='k',
            alpha=.25,
            zorder=1e5,
            s=100,
            clip_on=False,
        )

    summary = pd.Series({
        desc: np.mean(values)
        for desc, values in dots.items()
    })

    ax.plot(
        np.arange(len(summary)),
        summary,
        '_-',
        color='xkcd:cyan',
        zorder=1e6,
        linewidth=.25,

    )

    ax.set_xticks(np.arange(len(dots)))
    ax.set_xticklabels([
        k.replace('_', '\n').replace('unilat-cap', 'unilat\ncap')
        for k in dots.keys()
    ], fontsize=6)

    stats = ', '.join([f'n{i}={len(values)}' for i, values in enumerate(dots.values())])
    stats = [stats]

    _, baseline = list(dots.items())[0]

    any_sig = False
    for i in range(1, len(dots)):
        _, values = list(dots.items())[i]

        u, p = scipy.stats.mannwhitneyu(baseline, values, alternative='two-sided')

        stats.append(f'U={u} p={p:.4f}{splot.p_value_stars(p)}')

        any_sig = any_sig or (splot.p_value_stars_level(p) != 0)

    stats = '\n'.join(stats)

    splot.add_desc(
        ax,
        stats,
        fontsize=4,
        loc='bottom right',
        bkg_color='none',
        color='k' if not any_sig else 'b',
    )

    ax.set(xlim=(-0.5, len(dots) - 0.5))


def plot_durations_global(reg_sel_multi, sleep_stats):
    stat_names = ['rem_total', 'sws_total', 'cycle_count', 'rem_median', 'sws_median', 'cycle_median', 'duty_median']

    f, axs = plt.subplots(ncols=len(stat_names) // 1, nrows=1, figsize=(10, 1.5))

    for i, stat in enumerate(stat_names):

        ax = axs.ravel()[i]

        plot_dots_multi(
            ax,
            {desc: sleep_stats.loc[desc, ('global', stat)] for desc in reg_sel_multi.keys()}
        )

        if stat in ['rem_total', 'sws_total']:
            splot.set_time_ticks(ax, which='y', scale='hours', tight=False, lim=(ms(hours=0), ms(hours=8)))

        if stat in ['rem_median', 'sws_median']:
            splot.set_time_ticks(ax, which='y', scale='seconds', tight=False, lim=(ms(seconds=0), ms(seconds=120)))

        if stat in ['cycle_median']:
            splot.set_time_ticks(ax, which='y', scale='seconds', tight=False, lim=(ms(seconds=60), ms(seconds=180)))

        if stat in ['duty_median']:
            ax.set(
                ylim=(0, 1),
                ylabel='ratio',
            )

        elif stat in ['acorr_interval']:
            splot.set_time_ticks(ax, which='y', scale='minutes', lim=(ms(minutes=1.5), ms(minutes=3)), tight=False)

        elif stat.endswith('_count'):
            ax.set(
                ylim=(100, 300),
                ylabel='count',
            )

        ax.set_title(stat.replace('_', ' '), fontsize=8)

        ax.tick_params(bottom=False)

    return f


def plot_dots_single(ax, early, late, is_time=True):
    xticks = {}

    x_noise = np.clip(np.random.normal(0, scale=.025, size=len(early), ), -.25, +.25)

    assert len(early) == len(late)

    ax.plot(
        np.array([
            np.zeros(len(early)) + x_noise,
            np.ones(len(early)) + x_noise],
        ),
        [early, late],
        '-',
        color='xkcd:silver',
        alpha=.5,
        linewidth=0.5,
    )

    ax.scatter(
        np.array([
            np.zeros(len(early)) + x_noise,
            np.ones(len(early)) + x_noise],
        ),
        [early, late],
        facecolor='k',
        alpha=.25,
        zorder=1e5,
        s=100,
        clip_on=False,
    )

    ax.plot(
        [0, 1],
        [early.median(), late.median()],
        '_-',
        color='xkcd:cyan',
        zorder=1e6,
        linewidth=.25,
    )

    xticks[0] = f'early'
    xticks[1] = f'late'

    ax.set_xticks(list(xticks.keys()))
    ax.set_xticklabels(list(xticks.values()))

    test = scipy.stats.wilcoxon(early, late)

    p_value = test.pvalue
    p_value_text = f'{p_value:.6f}'  # f'{p_value:.15f}'[:2+max(3, np.argmax([c != '0' for c in f'{p_value:.15f}'[2:]]))]

    desc = (
        f'n={len(early)}\n'
        f'W={test.statistic:g}\n'
        f'p={p_value_text} {splot.p_value_stars(p_value)}'
    )

    if is_time:
        desc += f'\nmedian diff = {(late - early).median() / 1000:g}s'
    else:
        desc += f'\nmedian diff = {(late - early).median():.3f}'

    any_sig = splot.p_value_stars_level(p_value) != 0

    splot.add_desc(
        ax,
        desc,
        fontsize=4,
        loc='bottom right',
        bkg_color='none',
        color='k' if not any_sig else 'b',
    )

    ax.set(xlim=(-0.5, +1.5))


def plot_durations_across_night(reg_sel_multi, sleep_stats):
    cats = ['rem', 'sws', 'cycle', 'duty']

    f, axs = plt.subplots(nrows=len(cats), ncols=len(reg_sel_multi), figsize=(4, 5), sharey='row')

    for j, desc in enumerate(reg_sel_multi.keys()):

        for i, cat in enumerate(cats):

            ax = axs[i, j]

            plot_dots_single(
                ax,
                sleep_stats.loc[desc, (f'early', f'{cat}_median')],
                sleep_stats.loc[desc, (f'late', f'{cat}_median')],
            )

            title = cat

            if cat in ['rem', 'sws']:
                splot.set_time_ticks(ax, which='y', scale='seconds', lim=(0, ms(minutes=2)), major=ms(seconds=30))
                title = f'{cat} median'

            if cat in ['cycle']:
                splot.set_time_ticks(ax, which='y', scale='seconds', lim=(ms(minutes=1), ms(minutes=3)),
                                     major=ms(seconds=30))
                title = f'{cat} median'

            elif cat in ['duty']:
                ax.set(ylabel='ratio', ylim=(0, 1))
                title = f'{cat} median'

            title = '\n' + title

            if i == 0:
                title = desc.replace('_', ' ') + f'\n{title}'

            ax.set_title(title, fontsize=8)

            ax.tick_params(bottom=False, labelsize=6)

    splot.drop_spines_grid(axs, bottom=False, bottom_label=False)

    return f


def take_lengths_shifted(
        exp_rem_wins,
        cat,
        shifts,
        #     length_range=(ms(seconds=20), ms(minutes=3)),
        #     length_range=(0, ms(minutes=3)),
        length_range,
        shuffle=False,
):
    all_shifted = {}

    for exp_name, rem_wins in exp_rem_wins.items():
        assert rem_wins.are_alternating()

        lengths = rem_wins.lengths()

        if shuffle:
            lengths = pd.Series(
                lengths.sample(frac=1, replace=False).values,
                index=rem_wins.index,
            )

        lengths = pd.DataFrame({
            shift: lengths.shift(shift)
            for shift in shifts
        })

        lengths[(lengths < length_range[0]) | (lengths > length_range[1])] = np.nan

        lengths = lengths[rem_wins['cat'] == cat]

        all_shifted[exp_name] = lengths

    return all_shifted


def calc_length_corrs(all_shifted):
    all_corrs = {}

    for exp_name, lengths in all_shifted.items():
        corrs = lengths.corr()

        corrs = corrs[0]

        all_corrs[exp_name] = corrs

    all_corrs = pd.DataFrame(all_corrs).T

    return all_corrs


def plot_all_corrs(all_corrs, ref_cat):
    clip_on = False

    f, ax = plt.subplots(figsize=(6, 1.75))

    ax.plot(
        all_corrs.columns,
        all_corrs.values.T,
        color='xkcd:grey',
        linewidth=0.5,
        alpha=.5,
        clip_on=clip_on,
    )

    for i, (col, vals) in enumerate(all_corrs.items()):
        ax.scatter(
            np.zeros(len(vals)) + col,
            vals,
            zorder=1e3,
            facecolor='k',
            edgecolor='w',
            linewidth=0.25,
            alpha=0.5,
            s=100,
            clip_on=clip_on,
        )

    other_cat = dict(rem='sws', sws='rem')[ref_cat]

    labels = [
        f'{ref_cat if (shift % 2) == 0 else other_cat}' + (f'\n{(shift + 1) // 2:+d}' if 0 != (shift + 1) // 2 else '')
        for shift in all_corrs.columns
    ]

    ax.set(
        xticks=all_corrs.columns,
        xticklabels=labels,
    )

    ax.plot(
        all_corrs.columns,
        all_corrs.mean().values,
        '_-',
        markersize=10,
        color='xkcd:magenta',
        zorder=1e3,
        clip_on=clip_on,
    )

    ax.set(
        ylim=(-.75, +1),
        ylabel='corr.',
    )

    y = .0

    for shift in all_corrs.columns[all_corrs.columns % 2 == 1]:
        ax.plot(
            [shift - .4, shift + 1.4],
            [y, y],
            clip_on=False,
            transform=ax.get_xaxis_transform(),
            color='k'
        )

        ax.text(
            shift,
            y,
            ' SW',
            transform=ax.get_xaxis_transform(),
            rotation=90,
            ha='center',
            va='bottom',
            fontsize=6,
        )
        ax.text(
            shift + 1,
            y,
            ' REM',
            transform=ax.get_xaxis_transform(),
            rotation=90,
            ha='center',
            va='bottom',
            fontsize=6,
        )

        cycle_idx = shift // 2 + 1
        if cycle_idx != 0:
            ax.text(
                shift + .5,
                y,
                f'\ncycle\n{cycle_idx:+d}',
                transform=ax.get_xaxis_transform(),
                fontsize=6,
                ha='center',
                va='top',
                clip_on=False,
            )

    splot.add_desc(ax, f'n={all_corrs.shape[0]:,g}')
    ax.axhline(0, color='k')

    ax.spines['bottom'].set_visible(False)
    ax.tick_params(bottom=False)

    splot.drop_spine(ax, 'x')
    ax.set(yticks=np.arange(-.75, 1.1, .25))

    return f


def plot_lengths(lengths, ref_cat, length_range):
    other_cat = dict(rem='sws', sws='rem')[ref_cat]

    pairs = {
        f'{ref_cat.upper()} vs prev {other_cat.upper()}': (0, -1),
        f'{ref_cat.upper()} vs next {other_cat.upper()}': (0, +1),
        f'{ref_cat.upper()} vs next {ref_cat.upper()}': (0, +2),
    }

    f, axs = plt.subplots(ncols=len(pairs), sharex='all', sharey='all', squeeze=False, figsize=(6, 1.75))

    axs = axs.ravel()

    for i, (title, (a, b)) in enumerate(pairs.items()):
        data = lengths[[a, b]].dropna()
        x = data[a]
        y = data[b]

        ax = axs[i]
        ax.scatter(
            x, y,
            facecolor='k',
            edgecolor='w',
            linewidth=0.25,
            alpha=0.5,
            s=50,
            clip_on=False,
        )

        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)

        splot.add_desc(
            ax,
            f'{splot.p_value_stars(p_value)} p={splot.format_p_value(p_value)}'
            f'\nr={r_value:.3f}'
            f'\nn={len(data):,g}',
            bkg_color='none',
            y=1,
        )

        splot.set_time_ticks(ax, which='x', scale='seconds')
        splot.set_time_ticks(ax, which='y', scale='seconds')

        ax.set(
            aspect='equal',
            xlim=(0, ms(minutes=3)),
            ylim=(0, ms(minutes=3)),
        )

        ax.plot(np.array(length_range), slope * np.array(length_range) + intercept, color='xkcd:magenta')

        ax.set(
            xlabel=title.split(' vs ')[0] + ' (sec)',
            ylabel=title.split(' vs ')[1] + ' (sec)',
            xlim=(length_range[0] - 10_000, length_range[1] + 10_000),
            ylim=(length_range[0] - 10_000, length_range[1] + 10_000),
        )

        ax.tick_params(left=True, labelleft=True)

    return f
