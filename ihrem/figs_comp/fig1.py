"""
Helper code to make panels for Figure 1 and related EDF.
See notebook for usage.
"""

import numpy as np
import pandas as pd
import scipy.stats
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import events
from ihrem import plot as splot
from ihrem import stacks, timeslice
from ihrem import trains
from ihrem.analysis import sne
from ihrem.timeslice import MS_TO_S, Win, ms


def plot_beta_vs_rate(sns, beta, example_win):
    rate = sns.get_rate(example_win, step=1000, rolling_win=10)
    rate = rate.dropna()

    f0, ax = plt.subplots(constrained_layout=True, figsize=(3, 1))

    ax.plot(example_win.crop_df(rate), color='k')
    splot.set_time_ticks(ax, major=ms(minutes=2), minor=ms(minutes=1), scale='seconds')

    ax.set(xlabel='min', ylabel='Hz')

    tax = ax.twinx()
    beta_color = 'r'
    tax.plot(example_win.crop_df(beta), color=beta_color)
    tax.set_ylabel('beta', color=beta_color)
    tax.spines['right'].set_visible(True)
    tax.spines['right'].set_color(beta_color)
    tax.tick_params(axis='y', colors=beta_color)

    return f0


def plot_sn_props(sns):
    f1, axs = plt.subplots(nrows=3, constrained_layout=True, figsize=(3, 3))

    ax = axs[0]
    isi = sns.get_inter_event_intervals().dropna()

    h, edges, _ = ax.hist(
        isi,
        facecolor='k',
        bins=np.arange(0, 151, 1),
    )

    isi_mode = (edges[np.argmax(h)] + edges[np.argmax(h) + 1]) * .5
    isi_mode = np.round(isi_mode, decimals=1)

    ax.text(
        isi_mode, 0.2,
        f'{isi_mode:,g}ms',
        transform=ax.get_xaxis_transform(),
        va='bottom',
        ha='center',
        color='w',
        fontsize=6,
    )

    ax.scatter(
        [isi_mode],
        [0.1],
        marker='v',
        transform=ax.get_xaxis_transform(),
        facecolor='w',
        edgecolor='none',
        s=10,
        zorder=1e6
    )

    ax.set(xlabel='IEI [ms]', ylabel='#SN')
    ax.spines['bottom'].set_position(('outward', 2))
    ax.tick_params(length=2, which='minor')
    splot.add_desc(ax, f'n={len(sns.reg):,g}')

    ax = axs[1]

    ax.hist(
        sns.reg['duration'],
        facecolor='k',
        bins=100,
    )
    ax.set(xlabel='duration [ms]', ylabel='#SN')
    ax.spines['bottom'].set_position(('outward', 2))

    ax = axs[2]

    ax.hist(
        sns.reg['amplitude'],
        facecolor='k',
        bins=100,
    )
    ax.set(xlabel='amplitude [uV]', ylabel='#SN')
    ax.spines['bottom'].set_position(('outward', 2))

    return f1


def take_stack(loader, ref_channel, picked, on='start_time', hz=(0, 100), baseline=(-1, 1)):
    s_raw = stacks.Stack.load_ms(
        loader,
        picked.reg[on],
        win_ms=(-500, +500),
        channels=[ref_channel],
    )

    s_raw = s_raw.isel(channel=0)

    s = s_raw.filter_pass(hz).reset_baseline(baseline)

    return s


def plot_example_traces(s):
    f, ax = plt.subplots(constrained_layout=True, figsize=(2, 1.75))

    ax.plot(
        s.coords['time'],
        s.values.T,
        color='xkcd:grey',
        alpha=.25,
        clip_on=False,
        linewidth=.5,
    )

    ax.plot(
        s.coords['time'],
        s.mean('event_id').values,
        color='k',
        alpha=1,
        clip_on=False,
    )

    splot.set_time_ticks(ax, major=50, minor=10)

    ax.autoscale(enable=True, axis='y', tight=True)
    ax.autoscale(enable=True, axis='x', tight=True)

    ax.set(xlabel='ms', ylabel='uV')

    splot.add_desc(ax, f'n={len(s.coords["event_id"]):,g}')

    ax.axhline(0, color='k', linewidth=.5)
    ax.axvline(0, color='k', linewidth=.5)

    return f


def load_sns_multiexp(reg, exp_chs, valid_win):
    exp_raw_sns = {}
    exp_beta = {}

    for (ref_ch, exp_name) in pbar(exp_chs):

        try:
            all_beta = reg.load_all_beta(exp_name, 'beta').T.reset_index(drop=True).T

            raw_sns = sne.SharpNegativeEvents(reg.load_all_sne(exp_name, suffix='_cdf'))
            raw_sns = raw_sns.patch_simplified_channels()

            raw_sns = raw_sns.sel_between(ref_time=valid_win)
            raw_sns = raw_sns.add_details(all_beta)

            exp_raw_sns[f'{exp_name} ch{ref_ch}'] = raw_sns.sel(channel=ref_ch)
            exp_beta[f'{exp_name} ch{ref_ch}'] = all_beta[ref_ch]

        except FileNotFoundError:
            pass

    return exp_raw_sns, exp_beta


def get_amp_bins(exp_sns, prop='amplitude', v_range=(-500, 10), v_step=10):
    """
    Extract reasonable bins to compute SN amplitude histograms.
    Note that, due to electrode sampling format (saved as fixed point), the recorded
    voltage is actually a discrete value, and windows that do not align to this
    can produce aliasing in the histogram (regular peaks/troughs).
    It's worse because each experiment has its own calibrated probe.
    Just using the minimum step seems to give reasonable results.
    """
    amp_step = {
        exp_name:
            sns[prop].sort_values().diff().replace(0, np.nan).min() for exp_name, sns in exp_sns.items()
    }

    amp_step = max(amp_step.values())

    amp_bins = np.arange(
        amp_step * np.ceil(v_range[0] / amp_step),
        amp_step * np.ceil(v_range[1] / amp_step),
        amp_step * np.ceil(v_step / amp_step))

    return amp_bins


def get_hists_iei(exp_sns, bins):
    hists = {
        key: np.histogram(
            sns.get_inter_event_intervals().values,
            bins=bins
        )[0]
        for key, sns in pbar(exp_sns.items(), total=len(exp_sns))
    }

    hists = pd.DataFrame(hists, index=pd.IntervalIndex.from_breaks(bins)).rename_axis(index='time', columns='exp')

    return hists


def get_hists_prop(exp_sns, name, bins):
    hists = {
        key: np.histogram(
            sns[name].values,
            bins=bins
        )[0]
        for key, sns in pbar(exp_sns.items(), total=len(exp_sns))
    }

    hists = pd.DataFrame(hists, index=pd.IntervalIndex.from_breaks(bins)).rename_axis(index='time', columns='exp')

    return hists


def plot_hists_multiexp(ax, hists, legend_loc='upper right', **set_kwargs):
    for key, trace in hists.items():
        ax.plot(
            trace.index.mid,
            trace,
            label=key,
            clip_on=False,
        )

    ax.set(
        ylabel='#SN',
        ylim=(0, hists.values.max() * 1.05),
        **set_kwargs,
    )

    ax.spines['bottom'].set_position(('outward', 2))

    if legend_loc is not None:
        ax.legend(
            loc=legend_loc,
            fontsize=4,
        )


def collect_rates(exp_sns, valid_win, exp_beta):
    exp_rates = {}

    for key, sns in exp_sns.items():
        assert len(sns) > 0

        rate = sns.get_rate(
            valid_win,
            step=1000,
            rolling_win=10,
        ).fillna(0)

        beta = exp_beta[key]

        beta = events.interpolate_trace(beta, rate.index)

        beta = beta / np.quantile(beta, .99)

        exp_rates[key] = pd.DataFrame({'beta': beta, 'rate': rate})

    return exp_rates


def plot_beta_vs_sn_corr(exp_rates):
    f, axs = plt.subplots(ncols=3, constrained_layout=True, figsize=(6, 2))

    ax = axs[0]

    all_lr = []

    for i, (key, df) in enumerate(exp_rates.items()):

        beta_range = np.array(
            [0, 1])  # exp_rates['beta', ref_ch, exp_name].min(), exp_rates['beta', ref_ch, exp_name].max()
        lr = scipy.stats.linregress(
            df['beta'].values.ravel(),
            df['rate'].values.ravel(),
        )

        all_lr.append(lr)

        if i == 0:
            ax.scatter(
                df['beta'],
                df['rate'],
                marker='.',
                facecolor='k',
                s=1,
            )
            ax = axs[0]
            ax.set(
                xlabel='norm. beta',
                ylabel='SN rate [Hz]',
                title=f'example ({key})',
            )

        #             ax.plot(
        #                 beta_range,
        #                 beta_range * lr.slope + lr.intercept,
        #                 color='k',
        #             )

        ax = axs[1]

        ax.plot(
            beta_range,
            beta_range * lr.slope + lr.intercept,
        )

        ax = axs[2]

        ax.scatter(
            lr.rvalue,
            [.01],
            marker='|',
            s=20,
            label=f'{key}',
            zorder=1e6,
            clip_on=True,
            transform=ax.get_xaxis_transform()
        )

    ax = axs[2]

    h = ax.hist([lr.rvalue for lr in all_lr], bins=np.linspace(0, 1, 11), facecolor='k')[0]

    #     ax.set_ylim(np.max(h) * -.1, np.max(h) * 3)

    ax.spines['left'].set_bounds(0., np.ceil(np.max(h) / 10) * 10)
    ax.set_yticks(np.arange(0, np.ceil(np.max(h) / 10) * 10 + 1, 5))

    ax = axs[1]
    ax.set(
        xlabel='norm. beta',
        ylabel='SN rate [Hz]',
        title='linear fit to beta and SN rate',
    )

    splot.add_desc(ax, f'n={len(all_lr)}', loc='upper left')

    ax = axs[2]
    ax.legend(loc='upper left', fontsize=4, ncol=1)
    ax.set(
        xlabel='correlation coefficient',
        ylabel='#exp',
        title="Pearson's r of linear fit"
    )
    ax.spines['bottom'].set_position(('outward', 2))

    return axs


# SNs vs Spikes

def load_example_spikes(reg, exp_name):
    spikes = trains.SpikeTrains.load_spikes_jrclust(
        reg.get_path(exp_name) / 'ironclust_hrs0-9' / 'binaryTest_nxpoly2_64ch.csv',
        win_ms=Win(ms(hours=0), ms(hours=9))
    )

    spikes.cells.sort_index(inplace=True)
    spikes.cells['side'] = 'right'
    spikes.cells.loc[[1, 2], 'side'] = 'left'

    spikes.cells['side'] = spikes.cells['side'].map({
        reg.loc[exp_name, 'side0']: 0,
        reg.loc[exp_name, 'side1']: 1,
    })

    return spikes


def load_example_sns(reg, exp_name):
    # note ironclust extracted only the first 9 hours!
    valid_win = Win(ms(hours=2), ms(hours=9))

    raw_all_sns = sne.SharpNegativeEvents(reg.load_all_sne(exp_name, suffix='_cdf'))
    raw_all_sns = raw_all_sns.patch_simplified_channels()
    raw_sns = raw_all_sns.sel_between(ref_time=valid_win)

    valid_sns = raw_sns.sel_between(null_cdf=(-np.inf, .025))
    return valid_sns


def get_spikes_within_sn(ch_sns, spks):
    sns_wins = ch_sns.to_wins()
    assert sns_wins.are_exclusive()

    classified_spikes = sns_wins.annotate_events(spks.spikes)
    classified_spikes = classified_spikes.dropna()

    return classified_spikes


def get_spike_count_per_sn(classified_spikes, ch_sns):
    counts = classified_spikes.groupby(['event_id', 'gid']).size().unstack('gid', fill_value=0)

    counts = counts.reindex(ch_sns.reg.index, fill_value=0)

    return counts


def report_probs(counts):
    totals = pd.DataFrame({gid: counts[gid].value_counts() for gid in counts.columns}).fillna(0).astype(int)
    # noinspection PyUnresolvedReferences
    assert (totals.sum().iloc[0] == totals.sum()).all()
    print('prob no spike (%):')
    print((totals / totals.sum()).loc[0] * 100)

    print('prob >= 1 spike (%):')
    print((totals / totals.sum()).loc[1:].sum() * 100)

    print('prob > 1 spike (%):')
    print((totals / totals.sum()).loc[2:].sum() * 100)


def get_psths(spikes, ref_times, psth_win, hz=True, tbin_size=.5):
    sn_wins = timeslice.Windows.build_around(ref_times, psth_win)
    #     sn_wins = sn_wins.drop_overlap()

    delays = sn_wins.classify_events(spikes.spikes['time'])

    delays['gid'] = spikes.spikes['gid'].reindex(delays.index.values)

    tbins = np.arange(psth_win[0], psth_win[1] + tbin_size, tbin_size)

    psths = pd.DataFrame({
        gid: np.histogram(dts, bins=tbins, density=False)[0]
        for gid, dts in delays.groupby('gid')['delay']
    })

    psths.index = pd.IntervalIndex.from_breaks(tbins)

    if hz:
        psths = (psths.T / (MS_TO_S * psths.index.length)).T

    return psths


def get_psths_multichan(spikes, valid_sns, psth_win, align_to='ref', per_sn=True, hz=True, tbin_size=.5):
    all_psths = {}

    for ch in valid_sns['channel'].unique():

        times = valid_sns.sel(channel=ch)[f'{align_to}_time']
        all_psths[ch] = get_psths(spikes, times, psth_win, hz=hz, tbin_size=tbin_size)

        if per_sn:
            all_psths[ch] = all_psths[ch] / len(times)

    return all_psths


def collect_example_traces(loader, valid_sns, align_to, psth_win, count=100):
    example_traces = {}

    for ch in 0, 1:
        example_traces[ch] = stacks.Stack.load_ms(
            loader,
            valid_sns.sel(channel=ch)[f'{align_to}_time'].sample(count),
            psth_win,
            channels=[ch],
            load_hz=1_000,
        )

        example_traces[ch] = example_traces[ch].isel(channel=0).zscore()

    return example_traces


def plot_psths(spikes, valid_sns, psths_multichan, example_traces, sn_desc='SNs', suptitle='', ylabel='#sp',
               figsize=(5, 5)):
    f, axs = plt.subplots(
        nrows=len(spikes.cells.index) + 1, sharex='all', sharey='row', ncols=2, constrained_layout=True,
        figsize=figsize,
        gridspec_kw=dict(height_ratios=[1.5] + [1] * len(spikes.cells.index))
    )

    for ch in [0, 1]:
        sn_count = len(valid_sns.sel(channel=ch))

        ax = axs[0, ch]
        ax.set_title(f'{sn_count:,g} {sn_desc}', fontsize=6)

        traces = example_traces[ch]

        for idx in traces.coords['event_id']:
            ax.plot(
                traces.sel(event_id=idx).to_series(),
                color=splot.CMAPS_CHANNEL[f'ch{ch}'](np.random.random() * .75 + .25),
                linewidth=.25,
                alpha=.25,
            )

        ax.plot(
            traces.mean('event_id').to_series(),
            color=splot.COLORS[f'ch{ch}_dark'],
            linewidth=.5,
            alpha=1,
        )
        splot.set_time_ticks(ax, major=20, minor=10)

        for i, gid in enumerate(spikes.cells.index):

            ax = axs[i + 1, ch]

            psth = psths_multichan[ch][gid]

            heights = psth.values

            ax.bar(
                psth.index.left,
                heights,
                align='edge',
                width=psth.index.length,
                facecolor=splot.COLORS[f'ch{ch}'] if spikes.cells.loc[gid, 'side'] == ch else splot.COLORS[
                    f'ch{ch}_dark'],
            )

            if ch == 0:
                splot.add_desc(ax, f'unit #{gid}', fontsize=4, loc='upper right')

            ax.set(xlim=(psth.index.min().left, psth.index.max().right))

            ax.axhline(np.median(heights), color='k', linestyle='--', linewidth=.25)
            splot.set_time_ticks(ax, major=20, minor=10)

    for ax in axs[1:, 0].ravel():
        ax.set(ylabel=ylabel)

    for ax in axs[-1, :].ravel():
        ax.set(xlabel='ms')

    for ax in axs.ravel():
        # ax.axvline(0, color='k', linestyle='--', linewidth=.25)
        ax.spines['bottom'].set_position(('outward', 2))

    f.suptitle(suptitle)

    return axs


def report_probs_table(counts):
    totals = pd.DataFrame({gid: counts[gid].value_counts() for gid in counts.columns}).fillna(0).astype(int)
    # noinspection PyUnresolvedReferences
    assert (totals.sum().iloc[0] == totals.sum()).all()

    probs = pd.DataFrame({
        'prob no spike (%)': (totals / totals.sum()).loc[0] * 100,

        'prob == 1 spike (%)': (totals / totals.sum()).loc[1] * 100,

        'prob >= 1 spike (%)': (totals / totals.sum()).loc[1:].sum() * 100,

        'prob > 1 spike (%)': (totals / totals.sum()).loc[2:].sum() * 100,
    })

    return probs
