import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.gridspec
import matplotlib.patches
import matplotlib.patheffects
import matplotlib.ticker
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import events
from ihrem import plot as splot
from ihrem import timeslice
from ihrem.analysis import switches as sw
from ihrem.timeslice import Win, ms


def patch_last_state(lead_traces):
    # fix last sample being skipped due to window edge comparison
    # on some extractions
    if np.count_nonzero(lead_traces['lead_state'].isna()) == 1:
        assert np.isnan(lead_traces['lead_state'].iloc[-1])

        lead_traces['lead_state'].fillna(method='ffill', inplace=True)
        assert np.count_nonzero(lead_traces['lead_state'].isna()) == 0


def filter_bad_rem(cycles, min_duration=ms(minutes=3)):
    rem_lengths = cycles.get_rem_wins().lengths()
    bad_rem = rem_lengths > min_duration
    count_bad = np.count_nonzero(bad_rem)
    print(
        f'Dropping {count_bad}/{len(rem_lengths)} ({100 * count_bad / len(rem_lengths):.1f}%) '
        f'suspiciously long REM periods'
    )
    return cycles.sel_mask_cycle(~bad_rem)


def plot_example_switch(full_xcorr, full_beta, lead_wins, zoom_win, highlight_wins, suptitle):
    shown_xcorr = full_xcorr.sel_between(time=zoom_win)
    shown_xcorr = shown_xcorr / np.quantile(np.abs(shown_xcorr.values), 0.999)

    shown_beta = pd.DataFrame({
        ch: events.interpolate_trace(full_beta[ch], shown_xcorr.coords['time'])
        for ch in ['ch0', 'ch1']
    })

    f, axs = plt.subplots(constrained_layout=True, nrows=2, sharex='all', figsize=(3, 2))

    ax = axs[0]
    for col, trace in shown_beta.items():
        ax.plot(trace.index - trace.index.min(), trace.values, color=splot.COLORS[col])

    ax.set_ylabel('norm.\nbeta')

    ax = axs[1]
    im = splot.plot_stack_2d(
        ax, shown_xcorr.reset_coord(),
        norm=matplotlib.colors.Normalize(-1, +1)
    )
    f.colorbar(im, ax=ax, aspect=20, extend='both')
    splot.set_time_ticks(ax, which='y', major=20, minor=10)

    ax.spines['bottom'].set_position(('outward', 6))

    splot.plot_wins_fill(
        ax,
        lead_wins.crop_to_main(zoom_win, reset='start'),
        ymax=-0.05,
        ymin=-0.1,
        alpha=1,
        clip_on=False,
    )

    ax = axs[0]
    for color, w in highlight_wins.items():
        ax.plot(w.shift(-zoom_win.start), [1, 1], color=color)

    f.suptitle(suptitle)

    return axs


def plot_state_space_with_traces(
        lead_traces, highlight_wins, xcol='log_lead_ch0', ycol='log_lead_ch1',
        downsample=None, rasterized=True, s=2 ** -6, alpha=1,
):
    axs = splot.plot_scat_with_marginals(
        lead_traces[[xcol, ycol]].iloc[::downsample],
        by=lead_traces['lead_state'].iloc[::downsample],
        s=s,
        alpha=alpha,
        xlabel='+20ms score',
        ylabel='-20ms score',
        density=False,
        rasterized=rasterized,
        edgecolor='none',
        marker='.',
    )

    axs['main'].set_aspect('equal')

    ax = axs['main']

    for color, highlight_win in highlight_wins.items():
        highlight_traces = highlight_win.crop_df(lead_traces)

        ax.plot(
            highlight_traces[xcol],
            highlight_traces[ycol],
            color=color,
        )

    return axs


def plot_lead_state_cycles(
        lead_state_cycles_wins, ax=None, xlim=(ms(minutes=-2.5), ms(minutes=+2.5)),
        xlabel='Time rel to REM [min]', ylabel='# REM cycles (chronologically)'
):
    if ax is None:
        f, ax = plt.subplots(constrained_layout=True, figsize=(2, 7))

    lead_state_cycles_wins.plot_cycles_stack(ax)

    ax.autoscale(enable=True, axis='y', tight=True)
    splot.set_time_ticks(ax, major=ms(minutes=1), minor=ms(minutes=.5), scale='seconds')

    ax.set(
        xlabel=xlabel, ylabel=ylabel,
        xlim=xlim,
    )

    ax.invert_yaxis()

    return ax


def plot_one_switch_dom_durations_simple(lengths, switch_counts):
    f, ax = plt.subplots(constrained_layout=True, figsize=(2, 2))

    # noinspection PyTypeChecker
    colors: pd.Series = (lengths['lead_ch0'] > lengths['lead_ch1'])
    colors = colors.map({
        True: splot.COLORS['ch0'],
        False: splot.COLORS['ch1'],
    })
    colors.loc[switch_counts > 0] = 'xkcd:dark grey'

    ax.scatter(
        lengths['lead_ch0'],
        lengths['lead_ch1'],
        c=colors,
        alpha=.75,
        clip_on=False,
        edgecolor='none',
    )

    splot.set_time_ticks(ax, which='x', scale='seconds')
    splot.set_time_ticks(ax, which='y', scale='seconds')
    ax.set(
        xlim=(0, ms(minutes=1.8)),
        ylim=(0, ms(minutes=1.8)),
    )

    ax.spines['left'].set_position(('outward', 4))
    ax.spines['bottom'].set_position(('outward', 4))

    ax.set(
        aspect='equal',
        xlabel=f'ch0 dom.',
        ylabel=f'ch1 dom.',
        title='REM dominance duration for\n0-switch (colored) or 1-switch (black)\n',
    )
    splot.add_desc(ax, f'n={len(lengths):,g}')

    return ax


def load_multiple_lead_traces_extended(reg, exp_names):
    # We work with data until light-on here (instead of the usual 2h-11h)
    # because we want to look at slow switching dynamics very late in the recording right before wake

    exp_lead_traces = {}

    for exp_name in pbar(exp_names):
        light_on = reg.get_events(exp_name)['light on']

        extended_win = Win(light_on - ms(hours=6), light_on)

        exp_lead_traces[exp_name] = sw.load_lead_traces(reg, exp_name, exp_valid_win=extended_win)
        patch_last_state(exp_lead_traces[exp_name])

    return exp_lead_traces


def get_cat_ratios(lead_wins, times):
    cat_samples = lead_wins.generate_cat(times).dropna()

    cat_flag = {}
    for cat in cat_samples.unique():
        cat_flag[cat] = cat_samples == cat

    cat_flag = pd.DataFrame(cat_flag)
    cat_ratio = cat_flag.astype(int)

    return cat_ratio


def plot_dominance_in_rem(lead_wins, cat_ratio, roll_win_ms, major=ms(hours=3), minor=ms(hours=1)):
    f, ax = plt.subplots(constrained_layout=True, figsize=(5, 1.5))

    sampling_period = np.diff(cat_ratio.index)
    assert np.allclose(sampling_period[0], sampling_period)
    sampling_period = sampling_period[0]

    roll_win_idcs = roll_win_ms / sampling_period
    assert roll_win_idcs.is_integer()
    roll_win_idcs = int(roll_win_idcs)
    cat_ratio_smooth = cat_ratio.rolling(roll_win_idcs, center=True, min_periods=1).mean().dropna()
    cat_ratio_smooth_rem = (
            cat_ratio_smooth.T / cat_ratio_smooth.loc[:, cat_ratio_smooth.columns != 'sws'].sum(axis=1)).T

    for cat in ['lead_ch0', 'lead_ch1']:
        ax.plot(cat_ratio_smooth_rem[cat] * 100, color=splot.COLORS[cat], linewidth=.5)

    splot.plot_wins_fill(
        ax,
        lead_wins.sel(cat='sws', but=True),

        ymin=0,
        ymax=.025,
        alpha=1,
    )

    splot.set_time_ticks(ax, major=major, minor=minor, scale='minutes')
    ax.set_xlim(cat_ratio.index.min(), cat_ratio.index.max())
    ax.set(
        ylabel='% of rem time',
        title=f'time spent in dominance\n({timeslice.strf_ms(roll_win_ms)} win)',
        ylim=(
            np.floor(cat_ratio_smooth_rem[['lead_ch0', 'lead_ch1']].values.min() * 100),
            np.ceil(cat_ratio_smooth_rem[['lead_ch0', 'lead_ch1']].values.max() * 100),
        )
    )

    # splot.plot_events_vline(ax, reg.get_events(exp_name, event_cols=['light off', 'light on']))

    return ax


def study_late_time_spent(reg, exp_name, lead_traces, time_pre_light=ms(hours=4)):
    lead_wins = timeslice.Windows.build_from_contiguous_values(lead_traces['lead_state'])

    zoom = Win.build_around(reg.get_events(exp_name)['light on'], pre=-time_pre_light)

    cat_ratio = get_cat_ratios(lead_wins, lead_traces.index)

    cat_ratio = zoom.crop_df(cat_ratio, reset=True)
    lead_wins = lead_wins.crop_to_main(zoom, reset=True)

    for hours in [.25]:
        ax = plot_dominance_in_rem(lead_wins, cat_ratio, roll_win_ms=ms(hours=hours), major=ms(hours=1),
                                   minor=ms(hours=.5))
        ax.set_ylim(0, 100)
        ax.set_yticks(np.arange(0, 101, 20))
        ax.set_yticks(np.arange(0, 101, 10), minor=True)
        ax.set_xlim(0, time_pre_light)
        ax.figure.savefig(f'figs/{exp_name}_rem_lead_percent_{hours:g}.pdf')


def load_multiple_exp_cycles(reg, all_exps):
    exp_lead_traces = {}
    for exp_name in pbar(all_exps, desc='load'):
        exp_lead_traces[exp_name] = sw.load_lead_traces(reg, exp_name)
        patch_last_state(exp_lead_traces[exp_name])

    exp_rem_wins = {}
    exp_lead_wins = {}

    for exp_name, lead_traces in pbar(exp_lead_traces.items()):
        lead_traces = exp_lead_traces[exp_name]
        rem_wins = timeslice.Windows.build_from_contiguous_values(lead_traces['rem_state'])

        lead_wins = timeslice.Windows.build_from_contiguous_values(lead_traces['lead_state'])

        exp_rem_wins[exp_name] = rem_wins
        exp_lead_wins[exp_name] = lead_wins

    exp_cycles_raw = {
        exp_name: sw.Cycles(
            sw.classify_wins_by_cycle(exp_lead_wins[exp_name], sw.extract_cycle_wins(exp_rem_wins[exp_name]))
        )

        for exp_name in pbar(all_exps)
    }

    exp_cycles_filtered = {
        exp_name: filter_bad_rem(cycles)
        for exp_name, cycles in exp_cycles_raw.items()
    }

    return exp_cycles_filtered


def get_samples_center_largest(cycles):
    main_centers_global = cycles.get_center_largest()[['lead_ch0', 'lead_ch1']]
    main_centers_relative = cycles.align_to_rem_center().get_center_largest()[['lead_ch0', 'lead_ch1']]

    df = pd.concat([
        main_centers_global.rename(columns=dict(lead_ch0='time_ch0', lead_ch1='time_ch1')),
        main_centers_relative.rename(columns=dict(lead_ch0='rel_ch0', lead_ch1='rel_ch1')),
    ],
        axis=1)

    return df.rename_axis(columns='')


def get_samples_center_sign(cycles):
    main_centers_global = pd.DataFrame({
        'time_ch0': cycles.get_rem_center(),
        'time_ch1': cycles.get_rem_center(),
    })

    main_centers_relative = cycles.align_to_rem_center().get_center_largest()[['lead_ch0', 'lead_ch1']]

    main_centers_relative['lead_ch0'] = main_centers_relative['lead_ch0'] >= main_centers_relative['lead_ch1']
    main_centers_relative['lead_ch1'] = ~main_centers_relative['lead_ch0']

    main_centers_relative = main_centers_relative.applymap(lambda x: {True: +1, False: 0}[x])

    df = pd.concat([
        main_centers_global.rename(columns=dict(lead_ch0='time_ch0', lead_ch1='time_ch1')),
        main_centers_relative.rename(columns=dict(lead_ch0='rel_ch0', lead_ch1='rel_ch1')),
    ],
        axis=1)

    return df.rename_axis(columns='')


def get_samples_length(cycles):
    main_centers_global = pd.DataFrame({
        'time_ch0': cycles.get_rem_center(),
        'time_ch1': cycles.get_rem_center(),
    })
    main_centers_relative = cycles.align_to_rem_center().get_period_lengths_per_cycle()[['lead_ch0', 'lead_ch1']]

    df = pd.concat([
        main_centers_global.rename(columns=dict(lead_ch0='time_ch0', lead_ch1='time_ch1')),
        main_centers_relative.rename(columns=dict(lead_ch0='rel_ch0', lead_ch1='rel_ch1')),
    ],
        axis=1)

    return df.rename_axis(columns='')


def get_samples_length_fraction(cycles):
    main_centers_global = pd.DataFrame({
        'time_ch0': cycles.get_rem_center(),
        'time_ch1': cycles.get_rem_center(),
    })
    main_centers_relative = cycles.align_to_rem_center().get_period_lengths_relative()[['lead_ch0', 'lead_ch1']]

    df = pd.concat([
        main_centers_global.rename(columns=dict(lead_ch0='time_ch0', lead_ch1='time_ch1')),
        main_centers_relative.rename(columns=dict(lead_ch0='rel_ch0', lead_ch1='rel_ch1')),
    ],
        axis=1)

    return df.rename_axis(columns='')


def get_rolled(samples, win_length=ms(minutes=12)):
    vmin = np.nanmin(samples[['time_ch0', 'time_ch1']].values)
    vmax = np.nanmax(samples[['time_ch0', 'time_ch1']].values)

    xvals = np.arange(vmin - 1_000, vmax + 1_000, 1_000)

    rolled = {}

    for ch in [0, 1]:
        rolled[ch] = timeslice.mean_roll_by(
            samples[f'time_ch{ch}'].values,
            samples[f'rel_ch{ch}'].values,
            win=Win.build_centered(0, win_length),
            xvals=xvals,
        )

    df = pd.DataFrame.from_dict(rolled)

    return df


def plot_samples_rolled(samples, rolled, exp_name, desc, ax=None):
    if ax is None:
        f, ax = plt.subplots(figsize=(7, 1.5), constrained_layout=True)

    for ch in [0, 1]:
        ax.scatter(
            samples[f'time_ch{ch}'],
            samples[f'rel_ch{ch}'],
            facecolor=splot.COLORS[f'ch{ch}'],
            clip_on=False,
            zorder=1e6,
            alpha=.5,
            edgecolor='none',
            marker='.',
        )

    for ch in [0, 1]:
        ax.plot(
            rolled[ch],
            color=splot.COLORS[f'ch{ch}'],
            linewidth=1,
            path_effects=[
                matplotlib.patheffects.Stroke(linewidth=1.5, foreground='black'),
                matplotlib.patheffects.Normal(),
            ]
        )

    # splot.set_time_axis(ax, which='y')
    # ax.set_ylabel('delay from\nrem center')

    ax.set_title(desc.replace('_', ' ') + f' ({exp_name})')

    splot.set_time_ticks(ax, major=ms(hours=1.5), scale=ms(minutes=1), minor=ms(minutes=30))
    ax.set_xlim(ms(hours=0), ms(hours=9))

    return ax


def extract_autocorr_pd(com, lags):
    max_lag = lags.max()

    # noinspection PyTypeChecker
    padded = pd.DataFrame(np.pad(com, [(max_lag, max_lag), (0, 0)]), columns=com.columns)

    autocorrs = pd.DataFrame.from_dict({
        cat: pd.Series({
            lag: trace.autocorr(lag)
            for lag in pbar(lags)
        })
        for cat, trace in padded.items()
    })

    return autocorrs


def extract_autocorr_pd_ms(traces, lags_ms):
    traces = traces - traces.mean()

    sampling_period_ms = np.diff(traces.index)[0]

    lags_steps = np.unique((lags_ms / sampling_period_ms).astype(int))

    autocorrs = extract_autocorr_pd(traces, lags_steps)

    autocorrs.index = autocorrs.index * sampling_period_ms

    return autocorrs


def plor_autocorr(autocorr, exp_name, title, ax=None):
    if ax is None:
        f, ax = plt.subplots(constrained_layout=True, figsize=(2, 2))

    for ch in 0, 1:
        ax.plot(
            autocorr[ch].index,
            autocorr[ch].values,
            color=splot.COLORS[f'ch{ch}']
        )

    ax.set_title(f'{title} ({exp_name})')

    ax.spines['bottom'].set_visible(False)
    ax.axhline(0, color='k')

    splot.set_time_ticks(ax, scale=ms(minutes=1), major=ms(minutes=30), minor=ms(minutes=10), label='min')

    return ax


def plot_all_autocorr_split_ch(exp_autocorr, desc):
    f, axs = plt.subplots(ncols=2, sharex='all', sharey='all', constrained_layout=True)
    f.suptitle(desc.replace('_', ' '))

    for ch in exp_autocorr.columns.get_level_values('channel').unique():
        ax = axs[ch]

        for exp_name, trace in exp_autocorr[ch].items():
            ax.plot(trace, label=exp_name, linewidth=.5)

        ax.plot(exp_autocorr[ch].mean(axis=1), zorder=1e6, color=splot.COLORS[f'ch{ch}_dark'], linewidth=1.5,
                linestyle='--')

        ax.set_title(f'ch{ch}')

    for ax in axs.ravel():
        splot.set_time_ticks(ax, scale=ms(minutes=1), major=ms(minutes=30), minor=ms(minutes=10), label='min')
        ax.spines['bottom'].set_visible(False)
        ax.axhline(0, color='k')

        ax.set_yticks([0, .5, 1])

    ax = axs[0]
    ax.legend(loc='upper right', fontsize=6)

    return axs


def plot_all_autocorr(exp_autocorr, desc):
    f, ax = plt.subplots(figsize=(3, 2), constrained_layout=True)
    f.suptitle(desc.replace('_', ' '))

    for (ch, exp_name), trace in exp_autocorr.items():
        ax.plot(trace, label=f'{exp_name} ch{ch}', linewidth=.5)

    ax.plot(exp_autocorr.mean(axis=1), zorder=1e6, color='k', linewidth=1.5, linestyle='--')

    splot.set_time_ticks(ax, scale=ms(minutes=1), major=ms(minutes=30), minor=ms(minutes=10), label='min')
    ax.spines['bottom'].set_visible(False)
    ax.axhline(0, color='k')

    ax.set_yticks([0, .5, 1])
    ax.legend(loc='upper right', fontsize=6)

    return ax


def study_all_center_of_main(exp_samples, desc):
    exp_autocorr = {}

    lag_range = ms(minutes=-120), ms(minutes=+120)
    # for exp_name, cycles in pbar(exp_cycles_filtered.items(), total=len(exp_cycles_filtered)):
    for exp_name, samples in pbar(exp_samples.items()):
        rolled = get_rolled(samples)
        ax = plot_samples_rolled(samples, rolled, exp_name, desc)
        ax.figure.savefig(f'figs/{exp_name}_{desc}.pdf')

        autocorr = extract_autocorr_pd_ms(rolled, lags_ms=np.arange(lag_range[0], lag_range[1] + 1, ms(minutes=.5)))
        exp_autocorr[exp_name] = autocorr

        ax = plor_autocorr(autocorr, exp_name, title=f'auto-corr of\nrolled {desc}'.replace('_', ' '))
        ax.set_xlim(*lag_range)
        ax.axvline(0, color='k')
        ax.figure.savefig(f'figs/{exp_name}_{desc}_autocorr.pdf')
