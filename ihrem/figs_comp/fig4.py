from pathlib import Path

import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.gridspec
import matplotlib.patches
import matplotlib.ticker
import numpy as np
import pandas as pd
import scipy.stats
from matplotlib import pyplot as plt

from ihrem import plot as splot
from ihrem import stacks, timeslice
from ihrem.analysis import xcorr_bestlags as xcb
from ihrem.timeslice import Win, ms

PROBE_CMAPS = {
    0: matplotlib.cm.get_cmap('Greens'),
    1: splot.CMAPS_CHANNEL['ch0'],
    2: matplotlib.cm.get_cmap('Greys'),
}

PROBE_COLORS = {
    0: 'xkcd:green',
    1: splot.COLORS[f'ch0'],
    2: 'xkcd:black',
}


def select_isolated_sns(all_sns):
    sns = all_sns.sel_mask(all_sns.get_time_to_closest_per_channel() > 100)

    sns = sns.sel_mask(sns.is_match_valid())

    good_match = (
            sns['match_ref_time_diff'].between(-25, -15) |
            sns['match_ref_time_diff'].between(+15, +25)
    )

    sns = sns.sel_mask(good_match)

    return sns


def take_sn_stacks(loader, sns, ref_ch, count=100, hz=(None, 100), align='ref'):
    s = {}

    for lead in [True, False]:
        sel = sns.sel(is_lead=lead, channel=ref_ch)

        sel = sel.sel_between(match_ref_time_diff=(-25, -15) if not lead else (+15, +25))

        ts = sel[f'{align}_time']

        print(len(ts))

        traces = stacks.Stack.load_ms(
            loader,
            ts.sample(min(count, len(ts)), replace=False),
            (-200, +200),
        )

        s[lead] = traces.filter_pass(hz).zscore()

    return s


def extract_lag_stats(exp_best_lags, qmin):
    stats = {}

    for (pair, exp), best_lags in exp_best_lags.items():
        best_lags = xcb.XCorrBestLags(best_lags)

        best_lags = best_lags.clip_extreme_lags()
        best_lags = best_lags.sel_xcorr_range_q(qmin=qmin)

        lag = best_lags['lag']

        # use only negative lags because we want to look at preceeding
        lag = lag[lag <= 0]

        stats[pair, exp] = lag.describe()
        mode = lag.mode()
        assert len(mode) == 1
        stats[pair, exp].loc['mode'] = mode.values[0]

    stats = pd.DataFrame(stats).sort_index(axis=1)

    return stats


def plot_xcorr_full(pair, exp_name, xcorr):
    desc = f'{exp_name} {pair}'

    axs = splot.plot_traces2d_wrapped(
        xcorr.downsample(10),
        show_colorbar=True,
        suptitle=f'{desc}',
    )

    ax = list(axs.values())[0]

    path = f'figs/{desc}_full_xcorr.pdf'.replace(' ', '_').replace('-', '_')
    print(f'Saving: {path}')
    ax.figure.savefig(path)


def plot_example_xcorr(beta, xcorr_combs, zoom_win):
    f, axs = plt.subplots(nrows=3, sharex='all', constrained_layout=True, figsize=(4, 3))

    beta = zoom_win.crop_df(beta, reset=True)
    ax = axs[0]

    for ch, trace in beta.items():
        ax.plot(
            trace,
            color=splot.COLORS[ch],
        )

    ax.set(ylabel='a.u.')
    ax.spines['left'].set_bounds(0, .5)
    ax.set_yticks([0, .5])
    # ax.tick_params(left=False)

    for i, (key, xcorr) in enumerate(xcorr_combs.items()):

        xcorr = xcorr.sel_between(time=zoom_win).shift_coord(time=-zoom_win.start)

        xcorr = xcorr / np.quantile(np.abs(xcorr.values), 0.999)

        ax = axs[i + 1]

        im = splot.plot_stack_2d(
            ax,
            xcorr,
            norm=matplotlib.colors.Normalize(-1, 1),
        )
        f.colorbar(im, ax=ax, extend='both')

        splot.add_desc(ax, key['pair'], loc='upper left')

        for sp in ax.spines.values():
            sp.set_visible(True)

        splot.set_time_ticks(ax, which='y', major=30)

    splot.set_time_ticks(axs[-1], scale=ms(seconds=1), major=ms(seconds=20), minor=ms(seconds=5), label='Time (s)')

    return axs


def take_xcorr_comparison(exp_xcorr, valid_win):
    scores = {}

    for key, xcorr in exp_xcorr.items():
        xcorr = xcorr.sel_between(time=valid_win)

        main_lag = xcb.XCorrBestLags.from_xcorr(xcorr).clip_extreme_lags()
        main_lag = main_lag.sel_xcorr_range_q(qmin=.75)['lag'].mode().iloc[0]

        scores[key['pair'], main_lag] = xcorr.sel(lag=main_lag).to_series()

    scores = pd.DataFrame(scores).rename_axis(columns=['pair', 'lag'])

    vmax = scores.quantile(.999)
    scores = scores / vmax

    return scores


def interp_df(self, df: pd.DataFrame, step, reset=None, shift=0, plus=False) -> pd.DataFrame:
    """
    Resample the data within the given window using linear interpolation.
    The result is similar to crop_df but the values are the result
    of an interpolation and the sampling rate of the result will be determined by this window,
    instead of being predetermined by the data original sampling.
    """

    new_time = arange(self, step, plus=plus)

    return interp_df_by(
        self,
        df=df,
        new_time=new_time,
        reset=reset,
        shift=shift
    )


def interp_df_by(self, df: pd.DataFrame, new_time, reset=None, shift=0) -> pd.DataFrame:
    if isinstance(df, pd.Series):
        return interp_df_by(self, df.to_frame(), new_time=new_time, reset=reset, shift=shift).iloc[:, 0]

    time = df.index - shift

    import scipy.interpolate
    lerp = scipy.interpolate.interp1d(
        time,
        df.values.T,
        kind='linear',
    )

    valid = Win(time.min(), time.max())
    new_time = valid.crop_ts(new_time)

    data = lerp(new_time).T

    if isinstance(reset, bool):
        reset = None if not reset else 'start'

    if reset is not None:
        reset = self.relative_time(reset)
        new_time = new_time - reset

    return pd.DataFrame(data, columns=df.columns, index=new_time)


def arange(self, step, plus=False):
    stop = self.stop

    if plus:
        stop = stop + step

    return np.arange(self.start, stop, step)


def plot_simple_scatter_quarters(
        exp_name, scores_full, rem_wins, show_cat,
        rasterized=False, thresh=.1, show_quarters=True
):
    cats = rem_wins.classify_events(scores_full.index.to_series())['cat']

    scores = scores_full[cats == show_cat]
    scores = scores.sample(frac=1, replace=False)

    xpair = 'CLA-BST ipsi'
    xlag = scores[xpair].columns[0]

    ypair = 'CLA-BST contra'
    ylag = scores[ypair].columns[0]

    f, ax = plt.subplots(constrained_layout=True, figsize=(3, 3))

    x = scores[xpair, xlag]
    y = scores[ypair, ylag]

    ax.scatter(
        x,
        y,
        marker='.',
        s=.125,
        facecolor='k',
        #         facecolor=cats.map(splot.COLORS),
        rasterized=rasterized,
    )

    ax.set(
        title=exp_name,
        aspect='equal',
        xlabel=f'{xlag:g}ms x-corr [a.u.]\n{xpair}',
        ylabel=f'{ylag:g}ms x-corr [a.u.]\n{ypair}',
        xlim=(-.75, 1.75),
        ylim=(-.75, 1.75),
    )

    linreg = scipy.stats.linregress(x, y)

    sampling = np.min(np.unique(np.diff(scores.sort_index().index)))

    splot.add_desc(
        ax,
        f"Pearson's r = {linreg.rvalue:.3f}\n"
        f'p = {linreg.pvalue}\n'
        f'duration: {timeslice.strf_ms(scores.index.max() - scores.index.min())}\n'
        f'{sampling:.0f} ms sampling\n'
        f'n = {len(x):,g} / {len(scores_full):,g}',
    )
    ax.set_xticks([0, 1])
    ax.set_yticks([0, 1])

    if show_quarters:
        counts = (scores >= thresh).value_counts()
        ratios = counts / np.sum(counts.values)

        for x in [-.25, 1]:
            for y in [-.25, 1]:
                ax.text(
                    x, y,
                    f'{ratios[x > 0, y > 0] * 100:.2f}%',
                    va='center', ha='center', fontsize=6,
                )

        ax.axvline(thresh, color='r', linestyle='--', linewidth=.5)
        ax.axhline(thresh, color='r', linestyle='--', linewidth=.5)

    return ax


def annotate_gids(reg, exp_name, all_spikes):
    """
    This modifies IN PLACE the cells table with a column "region"
    indicating if it was (manually) registered as belonging to a region.
    """
    annotation_path: list = list(reg.get_path(exp_name).glob(f'catgt_{exp_name}*/CLUSTER_ID_region.xlsx'))
    assert len(annotation_path) == 1
    annotation_path: Path = annotation_path[0]

    # noinspection PyArgumentList
    annotation = pd.read_excel(str(annotation_path)).dropna()

    regions = pd.Series('unknown', index=all_spikes.cells.index)

    for p in all_spikes.cells['probe'].unique():
        local_annotation = annotation[annotation['probe'] == p]

        local_annotation = local_annotation.set_index('local_gid')
        assert local_annotation.index.is_unique

        local_cells = all_spikes.cells[all_spikes.cells['probe'] == p]
        assert local_cells['local_gid'].is_unique

        local_regions = local_annotation['region'].reindex(local_cells['local_gid'], fill_value='unknown')

        regions.loc[local_cells.index] = local_regions.values

    all_spikes.cells['region'] = regions


def plot_f4k_win_simple(cla_beta, tr, f4k_win_ms):
    tr = tr.sel_between(time=f4k_win_ms).shift_time(-f4k_win_ms.start).sort_reset_gids()

    n_probes = tr.cells['probe'].nunique()

    f, axs = plt.subplots(
        nrows=n_probes + 2, sharex='all', figsize=(2.5, 3),
        constrained_layout=True,
        gridspec_kw=dict(height_ratios=[1.5, 1.5] + [2] * n_probes)
    )

    cla_beta = f4k_win_ms.crop_df(cla_beta, reset=True)

    ax = axs[0]
    ax.plot(cla_beta, color=PROBE_COLORS[1])
    ax.set(
        ylabel='norm.\nbeta',
    )

    for i, (p, ptr) in enumerate(tr.iter_cells_by('probe')):
        ax = axs[1]
        ax.set(ylabel='Hz')

        ptr = ptr.sort_reset_gids()

        activity = ptr.compute_activity_per_cluster(np.arange(*ptr.win_ms, ms(seconds=1)), rolling_wins=10, fr=True)

        ax.plot(
            activity.index.mid,
            activity.mean(axis=1),
            color=PROBE_COLORS[p],
        )

        ax = axs[i + 2]
        ax.set(ylabel='unit')

        ax.scatter(
            ptr.spikes['time'],
            ptr.spikes['gid'],
            marker='|',
            s=5,
            linewidth=.25,
            facecolor=PROBE_COLORS[p],
        )

        ymax = ptr.spikes['gid'].max() * 1.1

        ax.set_ylim(ptr.spikes['gid'].max() - ymax, ymax)

    axs[0].spines['bottom'].set_visible(False)
    axs[1].spines['bottom'].set_visible(False)

    for ax in axs[:-1]:
        ax.tick_params(bottom=False, which='both')

    for ax in axs[2:]:
        for sp in ax.spines.values():
            sp.set_visible(True)

    splot.set_time_ticks(axs[-1], scale=ms(seconds=1), major=ms(seconds=20), label='seconds')
    ax.set_xlim((0, f4k_win_ms.length))

    return axs


def get_activity_probes(good_spikes, valid_win, step_ms=100, rolling_ms=ms(seconds=10)):
    if rolling_ms is not None:
        assert rolling_ms % step_ms == 0
        rolling_wins = int(rolling_ms // step_ms)
    else:
        rolling_wins = None

    print((good_spikes.cells.groupby(['probe', 'region']).size()))

    valid_win_extended = valid_win.extend(ms(seconds=-10), ms(seconds=+10))

    full_act_imc = good_spikes.compute_activity_per_cluster(
        np.arange(*valid_win_extended, step_ms),
        rolling_wins=rolling_wins,
        fr=True,
    )

    full_act_imc_probes = full_act_imc.groupby(good_spikes.cells['probe'], axis=1).mean()
    full_act_imc_probes.index = full_act_imc_probes.index.mid

    full_act_imc_probes = interp_df(valid_win, full_act_imc_probes, step=100, plus=True)

    return full_act_imc_probes


def plot_example_wins(example_wins, full_act_probes, rem_wins, f4k_win_ms):
    f, axs = plt.subplots(
        nrows=len(example_wins),
        constrained_layout=True,
        squeeze=False,
        sharex='all',
        sharey='all',
        figsize=(4, 3),
    )

    axs = axs.ravel()

    for ax in axs:
        ax.set(ylabel='Hz')

    for i, (ax, win) in enumerate(zip(axs, example_wins)):

        traces = win.crop_df(full_act_probes, reset=True)

        for p, trace in traces.items():
            ax.plot(
                trace,
                color=PROBE_COLORS[p],
            )

        highlight = f4k_win_ms.clip(win).shift(-win.start)

        splot.add_desc(ax, f'{win}', loc='upper left')

        ax.set_yticks([0, 10])
        ax.spines['left'].set_bounds(0, 10)

        if highlight.length > 0:
            ax.fill_between(highlight, 0, 1, facecolor='xkcd:silver', transform=ax.get_xaxis_transform())

        shown_wins = rem_wins.crop_to_main(win, reset=True)
        splot.plot_wins_line(
            ax, shown_wins, yval=0,
            transform=ax.get_xaxis_transform(),
            clip_on=False,
        )

    for ax in axs[:-1]:
        ax.spines['bottom'].set_visible(False)
        ax.tick_params(bottom=False, which='both')

    axs[-1].spines['bottom'].set_position(('outward', 2))

    splot.set_time_ticks(axs[-1], scale=ms(minutes=1), major=ms(minutes=1), minor=ms(seconds=10), label='minutes')
    # axs[-1].autoscale(enable=True, axis='x', tight=True)
    axs[-1].set_xlim(0, max(win.length for win in example_wins))

    return axs


def plot_2d_single(reg, exp_name, ax, full_act_probes_full, probe_x, probe_y, rem_wins, show_cat, s=.25):
    probe_names = {
        i: reg.loc[exp_name, f'side{i}'][0] + reg.loc[exp_name, f'probe{i}'] for i in range(3)
    }

    cats = rem_wins.classify_events(full_act_probes_full.index.to_series())['cat']
    full_act_probes = full_act_probes_full[cats == show_cat]

    full_act_probes = full_act_probes.sample(frac=1, replace=False)

    x = full_act_probes.loc[:, probe_x]
    y = full_act_probes.loc[:, probe_y]

    ax.scatter(
        x.values,
        y.values,
        s=s,
        facecolor='k',
        rasterized=True,
    )

    ax.set_xlabel(
        f'{probe_names[probe_x]} (Hz)',
        color=PROBE_COLORS[probe_x],
    )

    ax.set_ylabel(
        f'{probe_names[probe_y]} (Hz)',
        color=PROBE_COLORS[probe_y],
    )
    ax.set(aspect='equal')

    linreg = scipy.stats.linregress(x, y)

    sampling = np.min(np.unique(np.diff(x.sort_index().index)))

    splot.add_desc(
        ax,
        f"Pearson's r = {linreg.rvalue:.3f}\n"
        f'p = {linreg.pvalue}\n'
        f'n = {len(x):,g}/{len(full_act_probes_full):,g}\n'
        f'duration: {timeslice.strf_ms(x.index.max() - x.index.min())}\n'
        f'{sampling:.0f} ms sampling',
    )

    ax.tick_params(labelleft=True, labelbottom=True)


def plot_sn_stacks(s, ref_ch):
    f, axs = plt.subplots(ncols=2, nrows=3, constrained_layout=True, figsize=(2.5, 3), sharex='all', sharey='all',
                          gridspec_kw=dict(height_ratios=[1, 1, 1]))

    for i, lead in enumerate([True, False]):

        for ch in [0, 1, 2]:

            ax = axs[ch, i]

            traces = s[lead].sel(channel=ch)

            traces = traces.reset_baseline().sel_between(time=(-50, +100))

            splot.add_desc(ax, f'n={traces.shape[0]}')

            ax.plot(
                traces.coords['time'],
                traces.values.T,
                color=splot.COLORS[f'ch{ch}'],
                linewidth=.25,
                alpha=.25,
            )

            ax.plot(
                traces.mean('event_id').to_series(),
                color=splot.COLORS[f'ch{ch}_dark'],
            )

            if i == 0:
                ax.set_ylabel('z-score')

        ax = axs[0, i]
        ax.set_title(f'ch{ref_ch} ' + ('leads' if lead else 'follows'))

    for ax in axs.ravel():
        ax.axvline(0, color='k', linewidth=.5)

    for ax in axs[-1, :]:
        splot.set_time_ticks(ax, major=40, label='ms')

    return f
