from pathlib import Path

import matplotlib
import matplotlib.cm
import matplotlib.colors
import matplotlib.gridspec
import matplotlib.patches
import matplotlib.ticker
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy import stats as st
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import stacks, timeslice
from ihrem.analysis import switches as sw
from ihrem.analysis import xcorr_bestlags as xcb
from ihrem.timeslice import ms

COND_COLORS = {'healthy': 'xkcd:blue', 'lesion': 'xkcd:red'}


def take_cla_bilat_area(probs, l0=.6, l1=np.inf):
    # default values skip the 0 bin

    return pd.DataFrame({
        'right': probs['CLA bilat'].loc[-l1:-l0].sum(),
        'left': probs['CLA bilat'].loc[l0:l1].sum(),
    })


def plot_stat_test(samples, ylim=(0, 1)):
    f, ax = plt.subplots(figsize=(2, 2), constrained_layout=True)

    for exp_name, vals in samples.T.items():
        ax.plot(
            vals,
            '.-',
            label=exp_name,
            clip_on=False,
        )

    ax.legend(loc='upper left', fontsize=4)
    ax.set(
        xlabel='dominance type',
        ylabel='ratio of lags',
        ylim=ylim,
        yticks=np.linspace(*ylim, 5),
        xlim=(-0.25, 1.25)
    )

    ax.spines['bottom'].set_position(('outward', 2))
    ax.spines['left'].set_position(('outward', 2))
    ax.tick_params(bottom=False)

    stat_test = st.wilcoxon(
        samples['left'],
        samples['right'],
        alternative='greater',
    )

    # noinspection PyUnresolvedReferences
    splot.add_desc(
        ax,
        f'T={stat_test.statistic}\np={stat_test.pvalue}\n'
        f'Wilcoxon signed-rank test', loc='lower right',
        bkg_color='none', fontsize=5
    )

    return ax


def plot_lesion_hists(reg, exp_names, fig_desc, min_xcorr_q, use_cache):
    exp_best_lags = xcb.compute_lags_best_xcorr(reg, exp_names, fig_desc=fig_desc, use_cache=use_cache)

    probs = xcb.compute_dist_best_lags(exp_best_lags, min_xcorr_q=min_xcorr_q)

    axs = xcb.plot_dists_best_lags(
        probs[['CLA bilat']],
        suptitle=f'{fig_desc}',
        figsize=(3, 2),
    )

    fig_path = Path(f'figs/{fig_desc}.pdf')
    print(f'Saving {fig_path.absolute()}')
    axs[0].figure.savefig(str(fig_path))


def plot_example_lesion_xcorr(xcorr, zoom_wins):
    f, axs = plt.subplots(nrows=2, sharex='all', constrained_layout=True, figsize=(5, 2))

    for i, zoom_win in enumerate(zoom_wins):

        assert zoom_win.length == ms(hours=1)

        shown_xcorr = xcorr.sel_between(time=zoom_win, reset=True)

        sel_xcorr = shown_xcorr / np.quantile(np.abs(shown_xcorr.values), .999)

        ax = axs[i]

        im = splot.plot_stack_2d(
            ax,
            sel_xcorr,
            norm=matplotlib.colors.Normalize(-1, 1)
        )

        if i == 1:
            f.colorbar(im, ax=axs, extend='both', aspect=70)

        for sp in ax.spines.values():
            sp.set_visible(True)

        splot.set_time_ticks(ax, which='y', major=20, label='lag (ms)')

        for t in -20, 20:
            ax.axhline(t, linestyle='--', color='k', linewidth=.5)

    axs[0].set_xlabel('')
    axs[-1].set_xlim(0, ms(hours=1))
    splot.set_time_ticks(axs[-1], major=ms(minutes=10), scale=ms(minutes=1))

    return axs


def load_exp_all_beta_norm_to_min(reg, exp_names):
    # Note that due to lesion, beta is ALWAYS higher on one channel than the other
    # so normalizing to the max for each channel independently actually removed this
    # difference.
    # Instead, normalize here to the min (well to a small quantile)

    exp_beta = {}

    for exp_name in pbar(exp_names):
        all_beta = reg.load_all_beta(exp_name, 'beta').droplevel('channel', axis=1)
        all_beta = all_beta / all_beta.quantile(1 - .999)

        exp_beta[exp_name] = all_beta

    return exp_beta


def plot_xcorr_sel(xcorr, zoom_win, betas, lag_win=(-60, 60), downsample=10, major=ms(minutes=2)):
    f, axs = plt.subplots(nrows=2, sharex='all', sharey='none', constrained_layout=True, figsize=(8, 3))

    ax = axs[0]
    for ch, trace in betas.items():
        ax.plot(
            zoom_win.crop_df(trace, reset=True),
            color=splot.COLORS[f'ch{ch}'],
            label=f'ch{ch}',
        )

    ax.legend(loc='upper left', fontsize=6)
    ax.set_ylabel('norm.\nbeta')

    ax = axs[1]

    xcorr = xcorr.sel_between(time=zoom_win, reset=True)
    xcorr = xcorr.sel_between(lag=lag_win)

    splot.plot_stack_2d(ax, xcorr.downsample(downsample))
    ax.set_xlabel('')

    ax = axs[-1]
    splot.set_time_ticks(ax, major=major, scale=ms(minutes=1), label='min')

    return axs


def load_all_exp_remsws_wins(reg, exp_comp, valid_win):
    exp_remsws_wins = {}
    exp_betas = {}

    for cond, exp_names in exp_comp.items():
        for exp_name in pbar(exp_names):
            all_beta = reg.load_all_beta_norm(exp_name, area='CLA', exp_valid_win=valid_win)

            exp_remsws_wins[cond, exp_name] = sw.extract_rem_wins_from_log_beta_thresh_detour(
                all_beta,
                beta_thresh=all_beta['beta_max'].min() + .2,
                max_detours=ms(seconds=10),
            )

            exp_betas[cond, exp_name] = all_beta

    exp_remsws_wins = stacks.StackSet.from_dict(exp_remsws_wins, names=['cond', 'exp_name'])
    exp_betas = stacks.StackSet.from_dict(exp_betas, names=['cond', 'exp_name'])

    return exp_betas, exp_remsws_wins


def plot_beta_by_rem_state_all(exp_betas, exp_rem_wins, shown_win):
    f, axs = plt.subplots(nrows=len(exp_rem_wins), sharex='all', sharey='all', figsize=(7, .5 + .3 * len(exp_rem_wins)),
                          constrained_layout=True)

    i = 0

    for cond, exps in exp_rem_wins.iterby('cond'):
        for exp_name, rem_wins in exps.items():
            exp_name = exp_name.values[0]

            all_beta = exp_betas.get(cond=cond, exp_name=exp_name)

            ax = axs[i]
            splot.plot_trace_highlighted(
                ax,
                shown_win.crop_df(all_beta['beta_max'], reset=True),
                rem_wins.crop_to_main(shown_win, reset=True),
                styles={
                    'rem': dict(color=splot.COLORS['rem'], zorder=1e6),
                    'sws': dict(color=splot.COLORS['sws']),
                },
                clip_on=False,
                linewidth=.5
            )

            ax.spines['bottom'].set_visible(False)
            ax.tick_params(bottom=False, which='both')
            ax.spines['left'].set_visible(False)
            ax.tick_params(left=False, labelleft=False, which='both')

            splot.add_desc(ax, f'{cond}: {exp_name}', color=COND_COLORS[cond])

            #         ax.set_yticks([0, 1])
            #         ax.set_ylim(0, 1)

            i += 1

    ax = axs[-1]
    ax.spines['bottom'].set_visible(True)
    ax.tick_params(bottom=True, which='both')

    ax.spines['left'].set_visible(True)
    ax.spines['left'].set_bounds(0, 1)
    ax.tick_params(left=True, labelleft=True, which='both')

    splot.set_time_ticks(ax, major=ms(minutes=30), scale=ms(minutes=1), label='Time (min)', minor=ms(minutes=10))
    ax.set_xlim(0, shown_win.length)

    ax = axs[0]
    ax.set_title('norm. beta')

    return f


def describe_single_rem_stats(lengths):
    desc = lengths.describe()[['count', 'mean', 'std']]
    desc['total'] = lengths.sum()

    return desc


def collect_rem_stats(exp_rem_wins, cat):
    desc = pd.DataFrame.from_dict({
        tuple(sel.values): describe_single_rem_stats(wins.sel(cat=cat).lengths())
        for sel, wins in exp_rem_wins.items()
    })

    desc = desc.rename_axis(columns=exp_rem_wins.reg.columns).T.reset_index()

    return desc


def txt_describe_rem_stats(desc, cat, which='mean'):
    txt = ''

    txt += ', '.join(
        [f'{k} {which} {cat} {timeslice.strf_ms(v)}' for k, v in desc.groupby('cond')[which].mean().items()])

    t_statistic, p_value = st.ttest_ind(desc.loc[desc['cond'] == 'lesion', which],
                                        desc.loc[desc['cond'] == 'healthy', which], equal_var=False)

    txt += f"; P={p_value:.6f}, t={t_statistic:.3f}; Welch's t-test"

    print(txt)
