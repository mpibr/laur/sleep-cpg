"""
Helper code to make panels for Figure 2 and related EDF.
See notebook for usage.
"""

import matplotlib.colors
import matplotlib.patches
import numpy as np
import pandas as pd
import scipy.signal
import scipy.stats as st
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import stacks, timeslice
from ihrem.analysis import sleep
from ihrem.analysis import switches as sw
from ihrem.timeslice import ms, MS_TO_S, Win


def plot_specgram_single(ax, raw, cmap='cividis'):
    nperseg = 256
    nfft = nperseg

    mpl_specgram_window = plt.mlab.window_hanning(np.ones(nperseg))
    x = raw.values
    fs = raw.estimate_sampling_rate()

    f, t, sxx = scipy.signal.spectrogram(
        x, fs, detrend=False,
        nfft=nfft,
        window=mpl_specgram_window,
        nperseg=nperseg,
    )

    db = 10 * np.log10(sxx)

    im = ax.imshow(
        db,
        extent=(np.min(t), np.max(t), np.min(f), np.max(f)),
        origin='lower',
        clip_on=False,
        cmap=cmap,
        vmin=-25, vmax=+25,
    )

    ax.set(
        #         ylim=(0, low_hz),
        ylabel='Hz',
    )

    return im


def plot_specgram_single_mpl(ax, raw, low_hz=100, cmap='viridis'):
    x = raw.values
    fs = raw.estimate_sampling_rate()

    _, _, _, im = ax.specgram(
        x,
        Fs=fs,
        scale='dB',
        mode='psd',
        cmap=cmap,
    )
    ax.set(
        ylim=(0, low_hz),
        ylabel='Hz',
    )

    return im


def plot_specgram(raw, highlight_band='beta', cmap='plasma'):
    freq_min, freq_max = sleep.FREQ_BANDS.loc[highlight_band, ['freq_min', 'freq_max']]

    f, axs = plt.subplots(nrows=4, sharex='all', figsize=(6, 3), constrained_layout=True)

    for ax in axs[:-1]:
        ax.spines['bottom'].set_visible(False)
        ax.tick_params(bottom=False, which='both')

    for ch in 0, 1:
        ax = axs[ch * 2]
        ax.plot(raw.coords['time'] * MS_TO_S, raw.sel(channel=ch).values, color=splot.COLORS[f'ch{ch}'], linewidth=.25)

        ax.set(
            ylabel='uV',
        )

        ax = axs[ch * 2 + 1]
        im = plot_specgram_single(ax, raw.sel(channel=ch), cmap=cmap)

        f.colorbar(im, ax=ax, extend='both').set_label('dB')

        rect = matplotlib.patches.Rectangle(
            (-0.01, freq_min),
            1.02,
            freq_max - freq_min,
            facecolor='none',
            edgecolor='k',
            linewidth=.25,
            alpha=1,
            linestyle='--',
            transform=ax.get_yaxis_transform(),
            clip_on=False,
        )
        ax.add_artist(rect)

        ax.text(
            -0.01, (freq_max + freq_min) * .5,
            f'beta',
            fontsize=6,
            transform=ax.get_yaxis_transform(),
            clip_on=False,
            ha='right',
            va='center',
        )

    splot.set_time_ticks(ax, major=60 * 3, label='seconds')

    return f


def plot_detailed_example_win(betas, xcorr, win):
    plot_hz = 100

    f, axs = plt.subplots(nrows=2, sharex='all', constrained_layout=True, figsize=(9, 2),
                          gridspec_kw=dict(height_ratios=[2, 3]))

    xcorr = xcorr.sel_between(time=win, reset=True)
    betas = win.crop_df(betas, reset=True)

    xcorr = xcorr / np.quantile(np.abs(xcorr.values), 0.999)

    ax = axs[0]
    for ch, trace in betas.items():
        ax.plot(
            trace,
            color=splot.COLORS[ch],
        )
    ax.set(
        ylabel='norm.\nbeta\n(a.u.)',
    )

    ax.spines['bottom'].set_visible(False)
    ax.tick_params(bottom=False, which='both')

    ax = axs[1]

    im = splot.plot_stack_2d(
        ax,
        xcorr.downsample(plot_hz),
        norm=matplotlib.colors.Normalize(-1, 1),
    )
    f.colorbar(im, ax=ax, extend='both')

    splot.set_time_ticks(ax=ax, major=20, minor=10, which='y')
    ax.set(ylabel='lag (ms)')

    axs[-1].set_xlim(0, win.length)

    splot.set_time_ticks(axs[-1], major=ms(minutes=5), scale=ms(minutes=1), minor=ms(minutes=1), label='Time (min)')

    return axs


def plot_pairs_hist2d(ax, pairs, ref_channel, other_channel, vmax, lag_win=(-50, +50)):
    cmap_ch = {
        ch: matplotlib.colors.LinearSegmentedColormap.from_list('xcorr', [
            'w',
            splot.COLORS[f'ch{ch}'],
            splot.COLORS[f'ch{ch}_dark'],
        ])
        for ch in [0, 1]
    }

    for is_lead in [True, False]:
        lead_ch = ref_channel if is_lead else other_channel

        sel = pairs.sel(is_lead=is_lead)
        #         tbins = np.arange(-51.5, 0.51, 1) if not is_lead else np.arange(0.5, 52.5, 1)
        tbins = np.arange(-51, 0.1, 1) if not is_lead else np.arange(0, 52, 1)

        _, _, _, sm = ax.hist2d(
            sel['match_ref_time_diff'],
            sel['match_amplitude_zscored_diff'],

            cmap=cmap_ch[lead_ch],
            bins=(
                tbins,
                np.linspace(-4, 4, 101),
            ),
            vmin=0, vmax=vmax,
        )

        ax.figure.colorbar(sm, ax=ax, shrink=.5, location='left').set_ticks([0, vmax / 2, vmax])

        splot.add_desc(
            ax,
            f'n={len(sel):,g}',
            loc='lower right' if is_lead else 'lower left',
            color=splot.COLORS[f'ch{ref_channel}_dark'] if is_lead else splot.COLORS[f'ch{other_channel}_dark'],
            bkg_color='none',
        )

    ax.set(
        xlabel=r'$\Delta t$ (ms)',
        ylabel=r'$\Delta norm. amp.$ (s.d.)',
        xlim=lag_win,
    )

    ax.set(xlim=(-50, +50))

    splot.set_time_ticks(ax, major=20, minor=5, which='x')
    ax.autoscale(enable=True, axis='y', tight=True)


def plot_matched_amp_vs_time_diff(matched, ref_channel=0, vmax_2d=600):
    other_channel = [1, 0][ref_channel]

    pairs = matched.sel(channel=ref_channel)

    # pairs = pairs.sample(10000)

    f, axs = plt.subplots(
        constrained_layout=True,
        figsize=(3, 2.5),
        nrows=2, ncols=2,
        sharex='col', sharey='row',
        gridspec_kw=dict(height_ratios=[1, 3], width_ratios=[3, 1]),
    )
    axs[0, 1].axis('off')

    plot_pairs_hist2d(axs[1, 0], pairs, ref_channel, other_channel, vmax=vmax_2d)

    ax = axs[0, 0]
    for is_lead in [True, False]:
        sel = pairs.sel(is_lead=is_lead)
        ax.hist(
            sel['match_ref_time_diff'],
            bins=np.arange(-50, 51, 1),
            facecolor=splot.COLORS[f'ch{ref_channel if is_lead else other_channel}'],
            edgecolor='none',
            alpha=.75,
        )

    ax.spines['bottom'].set_position(('outward', 2))
    ax.set_ylabel('#pairs')

    ax = axs[1, 1]
    for is_lead in [True, False]:
        sel = pairs.sel(is_lead=is_lead)

        ch_label = f'ch{ref_channel if is_lead else other_channel}'

        ax.hist(
            sel['match_amplitude_zscored_diff'],
            bins=np.linspace(-4, 4, 101),
            facecolor=splot.COLORS[ch_label],
            alpha=.75,
            edgecolor='none',
            orientation='horizontal'
        )

        ax.scatter(
            [1],
            sel['match_amplitude_zscored_diff'].mean(),
            marker='<',
            facecolor=splot.COLORS[f'{ch_label}_dark'],
            edgecolor='none',
            transform=ax.get_yaxis_transform(),
        )
    ax.spines['left'].set_position(('outward', 2))
    ax.set_xlabel('#pairs')

    p0 = pairs.sel(is_lead=True)
    p1 = pairs.sel(is_lead=False)
    stat_test = st.mannwhitneyu(
        p0['match_amplitude_zscored_diff'],
        p1['match_amplitude_zscored_diff'],
    )

    splot.add_desc(
        ax,
        f'U={stat_test.statistic}\nU prop = {stat_test.statistic / (len(p0) * len(p1)):.3f}\np={stat_test.pvalue}',
        loc='lower left',
        bkg_color='none',
        fontsize=6,
        rotation=90,
    )

    f.suptitle(f'difference of amp. and delay (ch{ref_channel} as ref.)\n')

    return f


def collect_isolated_sns(matched, ref_channel=0, size_range=(-4, -3)):
    all_picked = {}

    for name, win in {'lead': (+15, +25), 'foll': (-25, -15)}.items():
        picked = matched.sel(channel=ref_channel).sel_between(match_ref_time_diff=win)

        to_next = picked.get_inter_event_intervals_between_channels(first_ch=ref_channel, second_ch=ref_channel)
        to_prev = to_next.shift(1) * -1
        isolated = (to_next > 200) & (to_prev < -200)

        isolated = isolated.reindex(picked.reg.index).fillna(False)
        picked = picked.sel_mask(isolated)

        if name == 'lead':
            picked = picked.sel_between(amplitude_zscored=size_range)
        else:
            picked = picked.sel_between(match_amplitude_zscored=size_range)

        all_picked[name] = picked.sample(100)

    return all_picked


def load_multiple_stacks(loader, all_picked, load_win):
    all_stacks = {}

    for name, picked in all_picked.items():
        all_stacks[name] = stacks.Stack.load_ms(
            loader,
            picked['ref_time'],
            win_ms=load_win,
            # load_hz=1000,
        )

    return all_stacks


def plot_sn_shapes(all_stacks, signal_std, load_win, exp_name, ref_channel=0):
    other_channel = [1, 0][ref_channel]

    f, axs = plt.subplots(constrained_layout=True, figsize=(4, 2), ncols=2, sharex='all', sharey='all')

    low_hz = 500

    for i, (name, s) in enumerate(all_stacks.items()):
        ax = axs[i]
        ax.set_title(f'ch{ref_channel} {name}', color=splot.COLORS[f'ch{ref_channel}_dark'])

        s = s.filter_pass((0, low_hz))
        s = s.reset_baseline()
        #     s = s.reset_baseline((-60, +20))

        for eid in pbar(s.coords['event_id']):
            for ch in [other_channel, ref_channel]:
                tr = s.sel(event_id=eid, channel=ch)
                tr = tr / signal_std[ch]

                ax.plot(
                    tr.coords['time'], tr.values,
                    color=splot.COLORS[f'ch{ch}'],
                    linewidth=.25,
                    alpha=.25,
                )

        for ch in [other_channel, ref_channel]:
            sel = s.sel(channel=ch) / signal_std[ch]
            ax.plot(
                sel.mean('event_id').to_series(),
                color=splot.COLORS[f'ch{ch}_dark'],
                zorder=1e6,
            )

        n = len(s.coords['event_id'])
        splot.add_desc(ax, f'{n:,g} pairs\n<{low_hz}hz')

        for ax in axs:
            splot.set_time_ticks(ax, major=20, minor=10)
            ax.tick_params(left=True, labelleft=True)
            ax.set(xlabel='lag [ms]', ylabel='z-score')
            ax.set_xlim(*load_win)

        f.savefig(f'figs/{exp_name}_sn_shapes.pdf')


def plot_example_xcorr(reg, exp_name, xcorr, desc, win_lag=(-50, +50), win_ms=Win(ms(hours=2), ms(hours=11))):
    xcorr = xcorr.sel_between(lag=win_lag, time=win_ms)
    xcorr = xcorr.downsample(1)
    xcorr = xcorr / np.quantile(np.abs(xcorr.values), 0.999)

    evs = reg.get_events(exp_name, ['light off', 'light on'])

    axs = splot.plot_traces2d_wrapped(
        xcorr,
        events=evs,
        show_colorbar=True,
        suptitle=f'{exp_name} ({desc})',
        tbin_width=ms(hours=3),
        norm=matplotlib.colors.Normalize(-1, +1),
        figsize=(9, 3),
    )

    tend = xcorr.coords['time'].max()

    for win, ax in axs.items():
        for sp in ax.spines.values():
            sp.set_visible(True)

        ax.axhline(-20, color='k', linewidth=.25)
        ax.axhline(+20, color='k', linewidth=.25)

        if win.contains(tend):
            ax.axvline(tend - win.start, color='k')

    ax = list(axs.values())[-1]
    splot.set_time_ticks(ax, scale=ms(minutes=1), label='minutes')

    path = f'figs/xcorr_{exp_name}_{desc}_{timeslice.strf_ms(win_ms.length)}.pdf'
    path = path.replace('-', '_').replace(':', '.').replace(' ', '_')
    print(f'Saving:\n{path}', flush=True)
    ax.figure.savefig(path)


#####################################################################################################################
# SWR independence


def plot_swr_detection(downsampled, all_beta, rem_wins, swr0, tbin_width=ms(minutes=6), which_ch=0):
    trace0 = downsampled[which_ch]

    resampled_beta = np.interp(downsampled.index, all_beta.index, all_beta['beta_max'].values)

    axs_dict = splot.plot_traces_wrapped(
        pd.DataFrame({
            f'ch{which_ch}': trace0,
            'beta': resampled_beta * 5 + 1,
        }),
        tbin_width=tbin_width,
        ylim=None,
        linewidth=.25,
        wins=rem_wins,
        figsize=(11, 5),
    )

    for tbin, ax in axs_dict.items():
        for cat, swr_group in swr0.groupby('cat'):
            ts = tbin.crop_ts(swr_group['time'], reset=0)
            ax.scatter(
                ts,
                swr_group.loc[ts.index, 'value'] * 1.5,
                marker='^',
                facecolor='k' if cat == 'sws' else 'r',
                s=3,
                edgecolor='none',
            )


def get_simple_loader(reg, exp_name):
    loader = reg.get_loader(exp_name)
    load_chans = loader.channel_probes_to_global(reg.get_probe_channels(exp_name, 'CLA'))
    loader = loader.sel_channels(load_chans)
    assert np.all(loader.channels.index.values == [0, 1])

    return loader


def extract_swr(reg, exp_name, process_win=None, only_sws=True, **kwargs):
    loader = get_simple_loader(reg, exp_name)

    main = stacks.Stack.load_single_ms(
        loader,
        process_win,
    )

    swr = sleep.find_sharp_waves(
        main,
        **kwargs,
    )
    assert swr.index.is_unique

    if only_sws:
        all_beta = reg.load_all_beta_norm(
            exp_name,
            add_max=True,
            exp_valid_win=process_win,
        )

        rem_wins = sw.extract_rem_wins_from_log_beta_thresh_detour(all_beta)

        swr['cat'] = rem_wins.classify_events(swr['time'])['cat']

        swr_sws = swr.query('cat == "sws"')

    # noinspection PyUnboundLocalVariable
    return swr_sws


def plot_swr_summary(ax, s):
    s_mean = s.mean('win_idx').to_dataframe2d().T
    s_std = s.std('win_idx').to_dataframe2d().T

    for ch, trace in s_mean.items():
        ax.plot(
            trace,
            color=splot.COLORS[f'ch{ch}'],
        )

        ax.fill_between(
            trace.index,
            trace - s_std[ch],
            trace + s_std[ch],
            facecolor=splot.COLORS[f'ch{ch}'],
            alpha=.25,
            edgecolor='none',
        )

    n = len(s.coords['win_idx'])
    splot.add_desc(
        ax,
        f'n={n:,g}',
        bkg_color='none',
        loc='upper right',
    )

    splot.set_time_ticks(ax, major=500, scale=ms(seconds=1))


def plot_swr_average(ax, exp_name, s):
    s = s.downsample(100)
    s_zscored = (s - s.mean(['win_idx', 'time'])) / s.std(['win_idx', 'time'])
    splot.add_desc(
        ax,
        exp_name,
        bkg_color='none',
        loc='upper left',
    )

    plot_swr_summary(ax, s_zscored)


def extract_swr_tdiffs(swr):
    t0s = swr.query('channel==0')['time'].values
    t1s = swr.query('channel==1')['time'].values

    all_cg = {
        'auto-correlogram (ch0)': (t0s[:, np.newaxis] - t0s[np.newaxis, :]),
        'auto-correlogram (ch1)': (t1s[:, np.newaxis] - t1s[np.newaxis, :]),
        'cross-correlogram': (t0s[:, np.newaxis] - t1s[np.newaxis, :]),
    }

    np.fill_diagonal(all_cg['auto-correlogram (ch0)'], np.nan)
    np.fill_diagonal(all_cg['auto-correlogram (ch1)'], np.nan)

    return all_cg


def plot_swr_correlograms(axs, all_cg):
    for i, (desc, tdiffs) in enumerate(all_cg.items()):
        ax = axs[i]

        side_ms = 9_000

        bin_width = 300
        h, edges = np.histogram(
            tdiffs.ravel(),
            bins=np.arange(-side_ms - bin_width * .5, +side_ms + bin_width, bin_width),
            density=False,
        )

        bin_widths_s = (edges[1:] - edges[:-1]) * timeslice.MS_TO_S

        h = (h / tdiffs.shape[0]) / bin_widths_s

        ax.bar(
            edges[:-1],
            h,
            align='edge',
            width=edges[1:] - edges[:-1],
            facecolor={
                'auto-correlogram (ch0)': splot.COLORS[f'ch0_dark'],
                'auto-correlogram (ch1)': splot.COLORS[f'ch1_dark'],
                'cross-correlogram': 'k',
            }[desc],
        )

        ax.axhline(np.median(h), linestyle='--', color='grey', linewidth=.5)
        ax.set(title=desc, ylabel='swr/s')
        ax.spines['left'].set_position(('outward', 2))
        ax.spines['bottom'].set_position(('outward', 2))

        splot.add_desc(
            ax,
            f'n={tdiffs.shape[1]:,g}',
            bkg_color='none',
            loc='lower right',
            color='w',
        )
        ax.set_xlim(edges.min(), edges.max())


def plot_swr_triggered(exp_swr, exp_swr_stack, s_win):
    f, axs = plt.subplots(constrained_layout=True, figsize=(11, 5), nrows=len(exp_swr_stack), ncols=4, sharex='col',
                          sharey='col', gridspec_kw=dict(width_ratios=[2, 1, 1, 1]))

    for i, (exp_name, s) in enumerate(pbar(exp_swr_stack.items(), total=len(exp_swr_stack))):
        plot_swr_average(axs[i, 0], exp_name, s)
        plot_swr_correlograms(axs[i, 1:], extract_swr_tdiffs(exp_swr[exp_name]))

    ax_corner = axs[-1, 0]
    ax_corner.set(
        xlim=s_win,
        xlabel='seconds',
        ylabel='z-scored',
    )

    for ax in axs[:, 0].ravel():
        if ax != ax_corner:
            ax.spines['left'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.tick_params(left=False, labelleft=False, bottom=False, labelbottom=False, which='both')

    for ax in axs[-1, 1:]:
        splot.set_time_ticks(
            ax,
            major=ms(seconds=3),
            scale=ms(seconds=1),
            label='seconds',
        )

    axs[0, 0].get_shared_x_axes().join(*axs[:, 1:].ravel())
    axs[0, 0].get_shared_y_axes().join(*axs[:, 1:].ravel())

    for ax in axs[:, 1:].ravel():
        ax.set_ylim(0, .5)

    return f
