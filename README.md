# Sleep Central Pattern Generator
=================================

Code acompanying the publication:

> Central pattern generator control of a vertebrate ultradian sleep rhythm \
> Fenk L. A., Riquelme J.L., and Laurent G. \
> Nature (2024) \
> https://doi.org/10.1038/s41586-024-08162-w




See also the previous version and article:

> https://gitlab.mpcdf.mpg.de/mpibr/laur/inter-hemispheric-rem
>
> Interhemispheric competition during sleep \
> Fenk L.A., Riquelme J.L., and Laurent G. \
> Nature (2023) \
> https://doi.org/10.1038/s41586-023-05827-w


Project Organization
--------------------

    ├── LICENSE
    ├── README.md
    ├── requirements.txt
    ├── setup.py                 <- Makes module installable with pip install -e .
    │    
    ├── ihrem                    <- Package encapsulating all code in this project.
    │    ├── extract.py          <- Main code to process raw data.
    │    ├── io_*.py             <- Code to load data recorded from Neuronexus and Neuropixel probes.
    │    ├── traces.py           <- Data container for collecitons of time-series data.
    │    ├── timeslice.py        <- Data container for sets of time windows.
    │    ├── figs_cpg            <- Code to generate figures for the 2024 CPG article.
    │    ├── figs_comp           <- Code to generate figures for the 2023 competition article.
    │    └── analysis
    │        ├── prc.py          <- Code for beta phase analysis.
    │        ├── stim.py         <- Code to analyse light pulse stimulation.
    │        ├── entrainment.py  <- Code to analyse trainse of pulses.
    │        └── sleep*.py       <- Code to extract beta power and detect Sharp Wave Ripples.
    │    
    └── notebooks
         ├── paper_2024          <- Jupyter notebooks to generate figures for this 2024 CPG article.
         ├── paper_2023          <- Jupyter notebooks to generate figures for the previous 2023 competition article.
         └── temp                <- Intermediate data that has been transformed through notebooks.


--------

You can install editable package as

    pip install -e .

Data is processed using `ihrem/extract.py` which orchestrates the analysis code in `ihrem/analysis`. See notebooks in `notebooks/figs` for examples of visualizing the processed results. Refer to the publication for data availability and detailed methods.
